﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;



namespace Ticket4MeDAL
{
    public class Ticket4MeDB
    {
        private SqlConnection conn;

        public Ticket4MeDB(string connectionString)
        {
            conn = new SqlConnection(connectionString);
        }

        #region Connection methods
        public void OpenConnection()
        {
            try
            {
                conn.Open();
            }
            catch (Exception ex)
            {
                throw new Exception("Error opening connection. " + ex.Message);
            }
        }

        public void CloseConnection()
        {
            try
            {
                conn.Close();
            }
            catch (Exception ex)
            {

                throw new Exception("Error closing the connection. " + ex.Message);
            }
        }
        #endregion

        #region Insert, Update, Delete
        public void InsertShow(int IDshow, string Performer, string Image, string Place, string City, string Country, string Day, DateTime Date, TimeSpan Time, decimal TicketPrice)
        {
            if (string.IsNullOrEmpty(Performer))//IF NULL CHECK!!!!!!!!!!!!!!!!!!!!!!!!!!!
            {
                throw new NullReferenceException("Performer is null or empty.");    
            }

            string sql = "INSERT INTO Table_AllNextShow (Performer, Image, Place, City, Country, Day, Date, Time, TicketPrice) VALUES (@Performer, @Image, @Place, @City, @Country, @Day, @Date, @Time, @TicketPrice)";
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                //Defind the parametrs:

                SqlParameter prm = new SqlParameter();
                prm.ParameterName = "@Performer";
                prm.Value = Performer;
                prm.SqlDbType = System.Data.SqlDbType.NVarChar;
                prm.Size = 50;
                cmd.Parameters.Add(prm);

                prm = new SqlParameter();
                prm.ParameterName = "@Image";
                prm.Value = Image;
                prm.SqlDbType = System.Data.SqlDbType.NVarChar;
                cmd.Parameters.Add(prm);

                prm = new SqlParameter();
                prm.ParameterName = "@Place";
                prm.Value = Place;
                prm.SqlDbType = System.Data.SqlDbType.NVarChar;
                prm.Size = 50;
                cmd.Parameters.Add(prm);

                prm = new SqlParameter();
                prm.ParameterName = "@City";
                prm.Value = City;
                prm.SqlDbType = System.Data.SqlDbType.NVarChar;
                prm.Size = 50;
                cmd.Parameters.Add(prm);

                prm = new SqlParameter();
                prm.ParameterName = "@Country";
                prm.Value = Country;
                prm.SqlDbType = System.Data.SqlDbType.NVarChar;
                prm.Size = 50;
                cmd.Parameters.Add(prm);

                prm = new SqlParameter();
                prm.ParameterName = "@Day";
                prm.Value = Day;
                prm.SqlDbType = System.Data.SqlDbType.NVarChar;
                prm.Size = 15;
                cmd.Parameters.Add(prm);

                prm = new SqlParameter();
                prm.ParameterName = "@Date";
                prm.Value = Date;
                prm.SqlDbType = System.Data.SqlDbType.Date;
                cmd.Parameters.Add(prm);

                prm = new SqlParameter();
                prm.ParameterName = "@Time";
                prm.Value = Time;
                prm.SqlDbType = System.Data.SqlDbType.Time;
                prm.Size = 15;
                cmd.Parameters.Add(prm);

                prm = new SqlParameter();
                prm.ParameterName = "@TicketPrice";
                prm.Value = TicketPrice;
                prm.SqlDbType = System.Data.SqlDbType.Money;
                cmd.Parameters.Add(prm);

                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (Exception)
                {
                    throw new Exception("Insert operation was failes.");
                }
            }
        }

        public void UpdateShow(int IDshow, string Performer, string Image, string Place, string City, string Country, string Day, DateTime Date, TimeSpan Time, decimal TicketPrice)
        {
            string sql = "UPDATE Table_AllNextShow SET Performer=@Performer, Image=@Image, Place=@Place, City=@City, Country=@Country, Day=@Day, Date=@Date, Time=@Time, TicketPrice=@TicketPrice WHERE IDshow=@IDshow";
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            { 
                            //Defind the parametrs:

                SqlParameter prm = new SqlParameter();
                prm.ParameterName = "@Performer";
                prm.Value = Performer;
                prm.SqlDbType = System.Data.SqlDbType.NVarChar;
                prm.Size = 50;
                cmd.Parameters.Add(prm);

                prm = new SqlParameter();
                prm.ParameterName = "@Image";
                prm.Value = Image;
                prm.SqlDbType = System.Data.SqlDbType.NVarChar;
                cmd.Parameters.Add(prm);

                prm = new SqlParameter();
                prm.ParameterName = "@Place";
                prm.Value = Place;
                prm.SqlDbType = System.Data.SqlDbType.NVarChar;
                prm.Size = 50;
                cmd.Parameters.Add(prm);

                prm = new SqlParameter();
                prm.ParameterName = "@City";
                prm.Value = City;
                prm.SqlDbType = System.Data.SqlDbType.NVarChar;
                prm.Size = 50;
                cmd.Parameters.Add(prm);

                prm = new SqlParameter();
                prm.ParameterName = "@Country";
                prm.Value = Country;
                prm.SqlDbType = System.Data.SqlDbType.NVarChar;
                prm.Size = 50;
                cmd.Parameters.Add(prm);

                prm = new SqlParameter();
                prm.ParameterName = "@Day";
                prm.Value = Day;
                prm.SqlDbType = System.Data.SqlDbType.NVarChar;
                prm.Size = 15;
                cmd.Parameters.Add(prm);

                prm = new SqlParameter();
                prm.ParameterName = "@Date";
                prm.Value = Date;
                prm.SqlDbType = System.Data.SqlDbType.Date;
                cmd.Parameters.Add(prm);

                prm = new SqlParameter();
                prm.ParameterName = "@Time";
                prm.Value = Time;
                prm.SqlDbType = System.Data.SqlDbType.Time;
                prm.Size = 15;
                cmd.Parameters.Add(prm);

                prm = new SqlParameter();
                prm.ParameterName = "@TicketPrice";
                prm.Value = TicketPrice;
                prm.SqlDbType = System.Data.SqlDbType.Money;
                cmd.Parameters.Add(prm);

                prm = new SqlParameter();
                prm.ParameterName = "@IDshow";
                prm.Value = IDshow;
                prm.SqlDbType = System.Data.SqlDbType.Int;
                cmd.Parameters.Add(prm);

                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (Exception)
                {
                    throw new Exception("Update was failes.");
                }
            }
        }

        public void DeleteShow(int IDshow)
        { 
            string sql = "DELETE FROM Table_AllNextShow WHERE IDshow=@IDshow";
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                SqlParameter prm = new SqlParameter();
                prm.ParameterName = "@IDshow";
                prm.Value = IDshow;
                prm.SqlDbType = System.Data.SqlDbType.Int;
                cmd.Parameters.Add(prm);

                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (Exception)
                {
                    throw new Exception("Delete wad failed.");
                }
            }
        }
        #endregion
    }
} 
