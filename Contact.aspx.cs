﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;


public partial class Contact : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            (Master as MyMasterStyle).ImageTitle = null;
        }

        successSent.Visible = false;

        if (Session["userFN"] != null)
        {
            txtFN.Text = Session["userFN"].ToString();
            txtLN.Text = Session["userLN"].ToString();
            txtEmail.Text = Session["userEmail"].ToString();
        }
    }

    protected void btnSend_Click(object sender, EventArgs e)
    {
        successSent.Visible = true;

        txtFN.Text = "";
        txtLN.Text = "";
        txtEmail.Text = "";
        txtComment.Text = "";
    }
}