﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Drawing;
using System.Web.UI.HtmlControls;

public partial class MyMasterStyle : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userFN"] == null)
        {
            BtnHello.ForeColor = Color.White;
            BtnHello.Text = "Hello Guest!";
            LiLogOut = false;
            LiNewLogin = true;
            BtnHello.Style.Add("cursor", "default");
        }
        else
        {
            if ((Session["userEmail"].ToString().ToUpper() == "ADMIN@ADMIN") && (Session["userPasswrd"].ToString().ToUpper() == "ADMIN"))
            {
                BtnHello.ForeColor = Color.Orange;
                BtnHello.Text = "Admin";
                LiNewLogin = false;
                LiLogOut = true;
                BtnHello.Style.Add("cursor", "pointer");
            }
            else
            {
                BtnHello.ForeColor = Color.Green;
                BtnHello.Text = "Hello " + Session["userFN"].ToString();
                LiNewLogin = false;
                LiLogOut = true;
                BtnHello.Style.Add("cursor", "pointer");

            }
        }
    }

    protected void btnLogOut_Click(object sender, EventArgs e)
    {
        ////clean session:
        Session["userEmail"] = null;
        Session["userFN"] = null;
        Session["userLN"] = null;
        Session["userPasswrd"] = null;
        Session.Abandon();

        BtnHello.ForeColor = Color.White;
        BtnHello.Text = "Hello Guest!";
        LiLogOut = false;
        LiNewLogin = true;
        BtnHello.Style.Add("cursor", "default");

        Response.Redirect("HomePage.aspx");
    }

    protected void btnHello_Click(object sender, EventArgs e)
    {
        if (btnHello.Text == "Admin")
        {
            Response.Redirect("~/Admin.aspx");
        }
        else if (Session["userFN"] != null)
        {
            Response.Redirect("~/EditUser.aspx");
        }
    }

    public string ImageTitle
    {
        get { return imgTitle.ImageUrl; }
        set { imgTitle.ImageUrl = value; }
    }

    //Hello:
    public Button BtnHello
    {
        get { return btnHello; }
        set { btnHello = value; }
    }

    //Log:
    public bool LiNewLogin
    {
        get { return liNewLogin.Visible; }
        set { liNewLogin.Visible = value; }
    }
    public bool LiLogOut
    {
        get { return liLogOut.Visible; }
        set { liLogOut.Visible = value; }
    }


    //searching:
    protected void UserSearch_OnTextChanged(object sender, EventArgs e)
    {
        Session["UserShearch"] = UserSearch.Text;
        Server.Transfer("SearchingPage.aspx");
    }

    //Sign In:
    protected void btnSignIn_Click(object sender, EventArgs e)
    {
        if (Session["userFN"] == null)
        {
            string CS = ConfigurationManager.ConnectionStrings["DBconnShows"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(CS))
            {
                SqlCommand comnd = new SqlCommand();
                comnd.Connection = conn;

                string sql = "SELECT * FROM Table_Users WHERE Email=@Email AND Password=@Password;";
                comnd.CommandText = sql;

                comnd.Parameters.AddWithValue("@Email", txtUserEmail.Text);
                comnd.Parameters.AddWithValue("@Password", txtUserPassword.Text);
                conn.Open();

                try
                {
                    lblEmailNotFoundOrPassNotCorrect.Visible = false;
                    comnd.ExecuteNonQuery();
                    Session["userEmail"] = txtUserEmail.Text;
                    Session["userPasswrd"] = txtUserPassword.Text;

                    using (SqlDataReader rdr = comnd.ExecuteReader())
                    {
                        if (rdr.HasRows)
                        {
                            while (rdr.Read())
                            {
                                Session["userFN"] = Convert.ToString(rdr["First Name"]);
                                Session["userLN"] = Convert.ToString(rdr["Last Name"]);
                            }
                        }
                    }

                    if ((Session["userEmail"].ToString().ToUpper() == "ADMIN@ADMIN") && (Session["userPasswrd"].ToString().ToUpper() == "ADMIN"))
                    {
                        BtnHello.ForeColor = Color.Orange;
                        BtnHello.Text = "Admin";
                        LiNewLogin = false;
                        LiLogOut = true;
                        BtnHello.Style.Add("cursor", "pointer");
                    }
                    else
                    {
                        BtnHello.Text = "Hello " + Session["userFN"].ToString();
                        BtnHello.ForeColor = Color.Green;
                        BtnHello.Style.Add("cursor", "pointer");
                        LiNewLogin = false;
                        LiLogOut = true;
                    }

                    string uri = HttpContext.Current.Request.Url.OriginalString;
                    if (uri == "http://localhost:51615/Ticketing.aspx")
                    {
                        Response.Redirect("~/Ticketing.aspx");
                    }
                    else if (uri == "http://localhost:51615/NewLogin.aspx")
                    {
                        Response.Redirect("~/HomePage.aspx");
                    }
                }
                catch (Exception)
                {
                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "Stay open", "<script language='javascript'> $('#liNewLogin').addClass('open'); </script>");
                    lblEmailNotFoundOrPassNotCorrect.Visible = true;
                    lblEmailNotFoundOrPassNotCorrect.Text = "* The email or password is incorrect.";
                }
            }
        }
        else
        {
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "Stay open", "<script language='javascript'> $('#liNewLogin').addClass('open'); </script>");
            lblEmailNotFoundOrPassNotCorrect.Visible = true;
            lblEmailNotFoundOrPassNotCorrect.Text = "* You already sign in!";
        }
    }
}
