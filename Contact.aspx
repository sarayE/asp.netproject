﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MyMasterStyle.master" AutoEventWireup="true" CodeFile="Contact.aspx.cs" Inherits="Contact" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script>
        $(document).ready(function () {
            $('#liHome liNewLogin liAllNextShows liLastShows').removeClass('active');

            $('#liContact').addClass('active');
        });
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <div class="container" style="width: 70%; margin: auto; margin-top: 10px;">

        <div class="well form-horizontal" id="contact_form">
            <fieldset>

                <!-- Form Name -->
                <legend>Contact Us Today!</legend>

                <!-- First Name-->
                <div class="form-group" style="margin-top: 20px;">
                    <label class="col-md-4 control-label">First Name</label>
                    <div class="col-md-6 inputGroupContainer">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            <asp:TextBox ID="txtFN" runat="server" name="first_name" placeholder="First Name" class="form-control" type="text"></asp:TextBox>

                            <div style="float: right; margin-right: 40px;">
                                <asp:RequiredFieldValidator ID="vldtxtFNEmpty" runat="server" ValidationGroup="vldSend" ControlToValidate="txtFN" Text="*" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                    </div>

                </div>

                <!-- Last Name-->
                <div class="form-group">
                    <label class="col-md-4 control-label">Last Name</label>
                    <div class="col-md-6 inputGroupContainer">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            <asp:TextBox ID="txtLN" runat="server" name="last_name" placeholder="Last Name" class="form-control" type="text" Style="float: left;"></asp:TextBox>

                            <div style="float: right; margin-right: 40px;">
                                <asp:RequiredFieldValidator ID="vldtxtLNEmpty" runat="server" ValidationGroup="vldSend" ControlToValidate="txtLN" Text="*" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Email-->
                <div class="form-group" style="margin-bottom: 40px;">
                    <label class="col-md-4 control-label">Email</label>
                    <div class="col-md-6 inputGroupContainer">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                            <asp:TextBox ID="txtEmail" runat="server" name="email" placeholder="Email Address" class="form-control" type="text"></asp:TextBox>

                            <div style="float: right; margin-right: 40px;">
                                <asp:RequiredFieldValidator ID="vldtxtEmailEmpty" runat="server" ValidationGroup="vldSend" ControlToValidate="txtEmail" Text="*" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <asp:RegularExpressionValidator ID="vldtxtEmailValid" runat="server" ValidationGroup="vldSend" ControlToValidate="txtEmail" Text="* Invalid Email" Display="Dynamic" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                    </div>
                </div>


                <!-- Comment -->
                <div class="form-group">
                    <label class="col-md-4 control-label">Comment</label>
                    <div class="col-md-6 inputGroupContainer">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span>
                            <asp:TextBox ID="txtComment" runat="server" class="form-control" name="comment" placeholder="write your comment..." TextMode="MultiLine"></asp:TextBox>

                        </div>
                        <div style="float: right; margin-right: 40px; width: 100%;">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:RequiredFieldValidator ID="vldtxtCommentEmpty" runat="server" ValidationGroup="vldSend" ControlToValidate="txtComment" Text="&nbsp;&nbsp;* Comment box cannot be empty." ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                </div>

                <!-- Success message -->
                <%--<div class="alert alert-success" role="alert" id="success_message">Success <i class="glyphicon glyphicon-thumbs-up"></i> Thanks for contacting us, we will get back to you shortly.</div>--%>

                <!-- Button -->
                <div class="form-group">
                    <label class="col-md-4 control-label"></label>
                    <div class="col-md-6">
                        <asp:LinkButton ID="btnSend" class="btn btn-warning" runat="server" OnClick="btnSend_Click" type="submit" ValidationGroup="vldSend">Send&nbsp;<span class='glyphicon glyphicon-send'></span>&nbsp;&nbsp;</asp:LinkButton>
                        <span id="successSent" class="label label-success" style="padding:10px;" runat="server" >Success to sent the message</span>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>


    <!-- /.container -->





</asp:Content>

