﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MyMasterStyle.master" AutoEventWireup="true" CodeFile="HomePage.aspx.cs" Inherits="HomePage" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script>
        $(document).ready(function () {
            $('#liHome').addClass('active');
        }); 
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

        <div class="starter-template" style="margin-top:-50px;">
            <h1>Ticket<span style="font-size: 1.5em">➃</span>Me</h1>
            <div style="align-content: center">
            </div>
            <br />
            <p class="lead">

                <b><u>TICKET PROTECT GUARANTEE</u></b><br/>
                •	100% authentic tickets and valid for entry to your event or full refund.<br/>
                •	You will receive your tickets on time for the event or full refund.<br/>
                •	You will receive a full refund if your event is canceled and not rescheduled.<br/>
                •	Our website is 100% secure and fraud free.<br/>
                <br/>
                <b><u>Top 3 Next concerts:</u></b>
            </p>
        </div>


    <!--The Circls-->
    <div class="container marketing" style="margin-top:30px;">

        <div style="text-align: center; margin:0 auto;">
            <div class="row" style="margin-right: 50px;">
           
                <!-- 1 -->
                <div class="col-lg-4">
                    <img class="img-circle" src="Images/JustinB.jpg" alt="performer" width="140" height="140"/>
                    <h2>Justin Bieber</h2>
                    <p><b>Rogers Arena</b> Vancouver, Canada <br /> Friday <br /> 3/11/2016 <br />7:30 PM</p>
                    <p><a class="btn btn-default" href="Ticketing.aspx" role="button">View tickets »</a></p>
                </div>

                <!-- 2 -->
                <div class="col-lg-4">
                    <img class="img-circle" src="Images/beonce.jpg" alt="Generic placeholder image" width="140" height="140"/>
                    <h2>Beyoncé</h2>
                    <p><b>Great Lawn at Central Park</b> Brooklyn, New York, USA <br /> Monday <br /> 3/6/2016 <br />8:00 PM</p>
                    <p><a class="btn btn-default" href="Ticketing.aspx" role="button">View tickets »</a></p>
                </div>

                <!-- 3 -->
                <div class="col-lg-4">
                    <img class="img-circle" src="Images/taylorS.jpg" alt="Generic placeholder image" width="140" height="140"/>
                    <h2>Taylor Swift</h2>
                    <p><b>Universal</b> Los-Angeles, california, USA <br /> Sunday <br /> 4/10/2016 <br />2:30 PM</p>
                    <p><a class="btn btn-default" href="Ticketing.aspx" role="button">View tickets »</a></p>
                </div>
            </div>
        </div>
    </div><!--The Circls END-->

</asp:Content>

