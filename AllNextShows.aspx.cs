﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

public partial class AllNextShows : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //Image:
            (Master as MyMasterStyle).ImageTitle = "Images/AllNextShowscrowd.jpg";
        }

        Get_Table_AllNextShow_FromDB();
    }

    private void Get_Table_AllNextShow_FromDB()
    {
        string CS = ConfigurationManager.ConnectionStrings["DBconnShows"].ConnectionString;
        using (SqlConnection conn = new SqlConnection(CS))
        {
            string sql = "SELECT Performer, Image, Place, City, Country, Day, Date, Time FROM Table_AllNextShow ORDER BY Date";
            SqlCommand comnd = new SqlCommand(sql, conn);

            conn.Open();

            using (SqlDataReader rdr = comnd.ExecuteReader())
            {
                TableRow Row = null;
                for (int i = 0; rdr.Read(); i++)
                {
                    string Performer = Convert.ToString(rdr["Performer"]);
                    string Image = Convert.ToString(rdr["Image"]);
                    string Place = Convert.ToString(rdr["Place"]);
                    string City = Convert.ToString(rdr["City"]);
                    string Country = Convert.ToString(rdr["Country"]);
                    string Day = Convert.ToString(rdr["Day"]);
                    DateTime Date = Convert.ToDateTime(rdr["Date"]);
                    TimeSpan Time = (TimeSpan)(rdr["Time"]);


                    if (Performer != null)
                    {
                        //ucNextShow:
                        UC_Show myShow = (UC_Show)LoadControl("~/UC/ucNextShow.ascx");

                        myShow.NextShowTitle = Performer;
                        myShow.NextShowImage = Image;
                        myShow.NextShowDetails = string.Format("<b>{0}</b> {1}, {2} <br/> {3} <br/> {4} <br/> {5}:{6} PM", Place, City, Country, Day, Date.ToShortDateString(), Time.Hours, Time.Minutes);

                        TableCell Cell = new TableCell();
                        Cell.Controls.Add(myShow);

                        if (i % 4 == 0)
                        {
                            Row = new TableRow();
                            TblNextShow.Rows.Add(Row);
                        }

                        Row.Cells.Add(Cell);
                    }
                }
            }
        }
    }
}