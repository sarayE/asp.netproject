﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MyMasterStyle.master" AutoEventWireup="true" CodeFile="NewLogin.aspx.cs" Inherits="SignIn" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script>
        $(document).ready(function () {
            $('#liHome').removeClass('active');

            $('#liNewLogin').addClass('active');
        });
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div style="width: 60%; margin: auto; margin-top: 10px;">
        <div class="panel panel-info" style="height:100%;">
                <div class="panel-heading">
                    <div class="panel-title">Join Us</div>
                    <div style="float: right; font-size: 90%; position: relative; top: -25px">Ticket<span style="font-size: 1.3em;"">➃</span>Me</div>
                </div>
            
                <div class="panel-body">
                    <br />
                    <!--Email-->
                    <div class="form-group">
                        <label for="email" class="col-md-3 control-label">Email</label>
                        <div class="col-md-9">
                        &nbsp;&nbsp;<asp:Label ID="lblEmailAlreadySing" runat="server" ForeColor="Red" style="font-size:0.9em;"></asp:Label>
                            <asp:TextBox ID="txtEmail" runat="server" class="form-control" name="email" placeholder="Email Adress" style="width: 70%; margin-left: 10px; margin-right: 10px; margin-bottom:25px; float:left;"></asp:TextBox>
                            <div style="width:100px; height:30px; float:left;">
                                <asp:RequiredFieldValidator ID="vldtxtEmailEmpty" runat="server" ControlToValidate="txtEmail" Text="*" ForeColor="Red" Display="Dynamic" ValidationGroup="vld"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="vldtxtEmailValid" runat="server" ControlToValidate="txtEmail" Text="* Invalid Email" ForeColor="Red" ValidationGroup="vld" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="Dynamic"></asp:RegularExpressionValidator>                               
                            </div>
                        &nbsp;</div>
                        <div style="margin-left: 306px">
                            </div>
                    </div>

                    <!--First Name-->
                    <div class="form-group">
                        <label for="firstname" class="col-md-3 control-label">First Name</label>
                        <div class="col-md-9">
                            <asp:TextBox ID="txtFN" runat="server" class="form-control" name="firstname" placeholder="First Name" style="width: 70%; margin-left: 10px; margin-right: 10px; margin-bottom:25px; float:left;"></asp:TextBox>
                            <div style="width:22px; height:30px; float:left;">
                                <asp:RequiredFieldValidator ID="vldtxtFNEmpty" runat="server" ControlToValidate="txtFN" Text="*" ForeColor="Red" ValidationGroup="vld"></asp:RequiredFieldValidator>
                            </div>
                        &nbsp;</div>
                    </div>

                    <!--Last Name-->
                    <div class="form-group">
                        <label for="lastname" class="col-md-3 control-label">Last Name</label>
                        <div class="col-md-9">                           
                            <asp:TextBox ID="txtLN" runat="server" class="form-control" name="lastname" placeholder="Last Name" style="width: 70%; margin-left: 10px; margin-right: 10px; margin-bottom:25px; float:left;"></asp:TextBox>
                            <div style="width:22px; height:30px; float:left;">
                                <asp:RequiredFieldValidator ID="vldtxtLNEmpty" runat="server" ControlToValidate="txtLN" Text="*" ForeColor="Red" ValidationGroup="vld"></asp:RequiredFieldValidator>
                            </div>
                        &nbsp;</div>
                    </div>

                    <!--Password-->
                    <div class="form-group">
                        <label for="password" class="col-md-3 control-label">Password</label>
                        <div class="col-md-9"> 
                            <asp:TextBox ID="txtPass"  runat="server" class="form-control" name="passwd" placeholder="Password"  style="width: 70%; margin-left: 10px; margin-right: 10px; margin-bottom:25px; float:left;" type="password"></asp:TextBox>
                            <div style="width:22px; height:30px; float:left;">
                                <asp:RequiredFieldValidator ID="vldtxtPassEmpty" runat="server" ControlToValidate="txtPass" Text="*" ForeColor="Red" ValidationGroup="vld"></asp:RequiredFieldValidator>
                            </div>
                        &nbsp;</div>
                    </div>
             
                    <div class="form-group">
                        <label for="password" class="col-md-3 control-label">Confirm Password</label>
                        <div class="col-md-9"> 
                            <asp:TextBox ID="txtPassConfirm"  runat="server" class="form-control" name="passwd" placeholder="Password"  style="width: 70%; margin-left: 10px; margin-right: 10px; margin-bottom:25px; float:left;" type="password"></asp:TextBox>
                            <div style="width:22px; height:30px; float:left;">
                                <asp:RequiredFieldValidator ID="vldtxtPassConfirmEmpty" runat="server" ControlToValidate="txtPassConfirm" Text="*" ForeColor="Red" Display="Dynamic" ValidationGroup="vld"></asp:RequiredFieldValidator>
                            </div>
                                <asp:CompareValidator ID="vldComparePass" runat="server" ControlToCompare="txtPass" ControlToValidate="txtPassConfirm" Text="* Passwords do not match" ForeColor="Red" Display="Dynamic" ValidationGroup="vld"></asp:CompareValidator>
                        &nbsp;</div>
                    </div>
 
           <div><!-- Button -->
                        <div class="col-md-offset-3 col-md-9">
                            <asp:Button ID="btnSignup" type="button" runat="server" Text="Sign Up" class="btn btn-info" style="width: 70%; margin:10px; margin-bottom:20px;" OnClick="btnSignup_Click" ValidationGroup="vld" AutoPostBack="true"/>
                            <br />
                            <span style="margin-left: 8px;">or</span>
                        </div>            
                        <div class="col-md-offset-3 col-md-9">
                            <div style="border-top: 1px solid #999; padding-top: 20px; width: 100%; margin-top:10px;" class="form-group">
                            <button id="btn-fbsignup" type="button" class="btn btn-primary" style="margin:0 auto;"><i class="icon-facebook"></i>Sign Up with Facebook</button>
                        </div>
                    </div>
               </div>

    </div>
    </div>
            </div>

 
</asp:Content>

