﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MyMasterStyle.master" AutoEventWireup="true" CodeFile="SearchingPage.aspx.cs" Inherits="SearchingPage" %>

<%@ Register Src="~/UC/ucLastShow.ascx" TagPrefix="uc1" TagName="ucLastShow" %>
<%@ Register Src="~/UC/ucNextShow.ascx" TagPrefix="uc1" TagName="ucNextShow" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <h3 style="text-align:center; margin:0 auto; background-color:#fcb273;"><u>Next shows result</u></h3>
    <br/>
    <br/>
    <asp:Table ID="TblNextShow" runat="server" style="margin: 0 auto;"></asp:Table>
    <p style="text-align:center; color:red; font-size:1.5em;"><asp:Label ID="lblResuleNext" runat="server"></asp:Label></p>    
    <hr/>
        <br/>
    <br/>
    <h3 style="text-align:center; margin:0 auto; background-color:#fcb273;"><u>Last shows result</u></h3>
    <br/>
    <br/>
    <asp:Table ID="TblLastShow" runat="server" style="margin: 0 auto;"></asp:Table>
    <p style="text-align:center; color:red; font-size:1.5em;"><asp:Label ID="lblResulePast" runat="server"></asp:Label></p>
    

</asp:Content>

