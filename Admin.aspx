﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MyMasterStyle.master" AutoEventWireup="true" CodeFile="Admin.aspx.cs" Inherits="Admin" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .auto-style1 {
            height: 20px;
            width: 230px;
        }

        .auto-style2 {
            height: 20px;
            width: 108px;
        }

        .auto-style3 {
            width: 108px;
        }

        .auto-style6 {
            width: 230px;
        }

        .auto-style7 {
            width: 231px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <%--Table_NextShows--%>
        <div class="panel panel-default">
            <div class="panel-heading">
                <b>Next shows</b>
                <asp:Label ID="lblNextshowsPanel" runat="server" Text="" Style="float: right;"></asp:Label>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <asp:GridView ID="gvAllNextShow" runat="server" ShowFooter="False" CssClass="table-hover table-striped" Style="margin: 0 auto;">
                        <Columns>
                            <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    &nbsp;&nbsp;<asp:CheckBox ID="ChkChooseShows_TblNext" runat="server" ValidationGroup="ck" Checked="False" />&nbsp;&nbsp;
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
                <br />
                <asp:Button ID="btnDel_TblNext" runat="server" Text="Delete" class="btn btn-outline btn-danger" Style="float: right; margin-right: 45px;" OnClick="btnDel_TblNext_Click" />
                <asp:Button ID="btnUpdate_TblNext" runat="server" Text="Update" class="btn btn-outline btn-warning" Style="float: right; margin-right: 15px;" OnClick="btnUpdate_TblNext_Click" />
                <asp:Button ID="btnInsert_TblNext" runat="server" Text="Insert" class="btn btn-outline btn-success" Style="float: right; margin-right: 15px;" OnClick="btnInsert_TblNext_Click" />
            </div>
            <div id="tbl_IUnext" runat="server">
                <table style="width: 100%; margin: 20px; margin-left: 70px;">
                    <tr>
                        <td class="auto-style2">
                            <b>&nbsp; &nbsp;IDshow:</b>
                        </td>
                        <td class="auto-style1">
                            <asp:TextBox ID="txt_IDshowNext" type="Number" runat="server" Width="200px" Style="margin-bottom: 5px;"></asp:TextBox>
                        </td>
                        <td class="auto-style3">
                            <asp:RequiredFieldValidator ID="vld_txt_PerformerNext" runat="server" ControlToValidate="txt_PerformerNext" Text="*" ForeColor="Red" ValidationGroup="vldGroup_tblNext"></asp:RequiredFieldValidator>
                            <b>Performer:</b>
                        </td>
                        <td class="auto-style7">
                            <asp:TextBox ID="txt_PerformerNext" runat="server" Width="200px" Style="margin-bottom: 5px;"></asp:TextBox>
                        </td>
                        <td class="auto-style3"><b>&nbsp; &nbsp;Image:</b></td>
                        <td>
                            <asp:TextBox ID="txt_ImageNext" runat="server" Width="200px" Style="margin-bottom: 5px;"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style3">
                            <asp:RequiredFieldValidator ID="vld_txt_PlaceNext" runat="server" ControlToValidate="txt_PlaceNext" Text="*" ForeColor="Red" ValidationGroup="vldGroup_tblNext"></asp:RequiredFieldValidator>
                            <b>Place:</b>
                        </td>
                        <td class="auto-style6">
                            <asp:TextBox ID="txt_PlaceNext" runat="server" Width="200px" Style="margin-bottom: 5px;"></asp:TextBox>
                        </td>
                        <td class="auto-style3">
                            <asp:RequiredFieldValidator ID="vld_txt_CityNext" runat="server" ControlToValidate="txt_CityNext" Text="*" ForeColor="Red" ValidationGroup="vldGroup_tblNext"></asp:RequiredFieldValidator>
                            <b>City:</b>
                        </td>
                        <td class="auto-style7">
                            <asp:TextBox ID="txt_CityNext" runat="server" Width="200px" Style="margin-bottom: 5px;"></asp:TextBox>
                        </td>
                        <td class="auto-style3">
                            <asp:RequiredFieldValidator ID="vld_txt_CountryNext" runat="server" ControlToValidate="txt_CountryNext" Text="*" ForeColor="Red" ValidationGroup="vldGroup_tblNext"></asp:RequiredFieldValidator>
                            <b>Country:</b>
                        </td>
                        <td>
                            <asp:TextBox ID="txt_CountryNext" runat="server" Width="200px" Style="margin-bottom: 5px;"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style3">
                            <asp:CompareValidator ID="vld_dd_DayNext" runat="server" ControlToValidate="dd_DayNext" Operator="NotEqual" Type="Integer" ValueToCompare="0" Text="*" ForeColor="Red" ValidationGroup="vldGroup_tblNext" />
                            <b>Day:</b>
                        </td>
                        <td class="auto-style6">
                            <asp:DropDownList ID="dd_DayNext" runat="server" Width="200px" Height="25px" Style="margin-bottom: 5px;">
                                <asp:ListItem Value="0">-Select a day-</asp:ListItem>
                                <asp:ListItem Value="1">Sunday</asp:ListItem>
                                <asp:ListItem Value="2">Monday</asp:ListItem>
                                <asp:ListItem Value="3">Tuesday</asp:ListItem>
                                <asp:ListItem Value="4">Wednesday</asp:ListItem>
                                <asp:ListItem Value="5">Thursday</asp:ListItem>
                                <asp:ListItem Value="6">Friday</asp:ListItem>
                                <asp:ListItem Value="7">Saturday</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td class="auto-style3">
                            <asp:RequiredFieldValidator ID="vld_txt_DateNext" runat="server" ControlToValidate="txt_DateNext" Text="*" ForeColor="Red" ValidationGroup="vldGroup_tblNext" Display="Dynamic"></asp:RequiredFieldValidator>
                            <b>Date:</b>
                        </td>
                        <td class="auto-style7">
                            <asp:TextBox ID="txt_DateNext" runat="server" Width="200px" Style="margin-bottom: 5px;"></asp:TextBox>
                        </td>                            
                        <td class="auto-style3">
                            <asp:RequiredFieldValidator ID="vld_txt_TimeNext" runat="server" ControlToValidate="txt_TimeNext" Text="*" ForeColor="Red" ValidationGroup="vldGroup_tblNext" Display="Dynamic"></asp:RequiredFieldValidator>                            
                            <b>Time:</b>
                        </td>
                        <td>
                            <asp:TextBox ID="txt_TimeNext" runat="server" Width="200px" Style="margin-bottom: 5px;"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style3">
                            <asp:RequiredFieldValidator ID="vld_txt_TicketPrice" runat="server" ControlToValidate="txt_TicketPrice" Text="*" ForeColor="Red" ValidationGroup="vldGroup_tblNext"></asp:RequiredFieldValidator>
                            <b>Ticket Price:</b>
                        </td>
                        <td class="auto-style6">
                            <asp:TextBox ID="txt_TicketPrice" runat="server" type="Number" Width="200px" Style="margin-bottom: 5px;"></asp:TextBox>$
                        </td>
                        <td><div style="margin-top:-20px;">&nbsp;<asp:Label ID="txt_DateNextError" runat="server" Text="" ForeColor="Red"></asp:Label></div></td>
                        <td></td>
                        <td><div style="margin-top:-20px;">&nbsp;<asp:Label ID="txt_TimeNextError" runat="server" Text="" ForeColor="Red"></asp:Label></div></td>
                        <td>
                            <asp:Button ID="btn_Cancel" runat="server" Text="Cancel" class="btn btn-outline btn-default" Style="margin-left: 60px;" OnClick="btn_Cancel_Click" />
                            <asp:Button ID="btn_InsertToDBnext" runat="server" Text="Insert" class="btn btn-outline btn-success" Style="margin-left: 5px;" ValidationGroup="vldGroup_tblNext" OnClick="btn_InsertToDBnext_Click" />
                            <asp:Button ID="btn_UpdateToDBnext" runat="server" Text="Update" class="btn btn-outline btn-warning" ValidationGroup="vldGroup_tblNext" OnClick="btn_UpdateToDBnext_Click" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <%--END Table_NextShows--%>
            <br />

    <%--Table_LastShows--%>
     <div class="panel panel-default">
        <div class="panel-heading">
            <b>Last shows</b>
            <asp:Label ID="lblLastshowsPanel" runat="server" Text="" Style="float: right;"></asp:Label>
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
            <div class="table-responsive">
                <asp:GridView ID="gvlastShows" runat="server" ShowFooter="False" CssClass="table-hover table-striped" Style="margin: 0 auto;">
                    <Columns>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                &nbsp;&nbsp;<asp:CheckBox ID="ChkChooseShows_TblLast" runat="server" ValidationGroup="ck" Checked="False" />&nbsp;&nbsp;
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
            <br />
            <asp:Button ID="btnDel_TblLast" runat="server" Text="Delete" class="btn btn-outline btn-danger" Style="float: right; margin-right: 45px;" OnClick="btnDel_TblLast_Click"  />
            <asp:Button ID="btnUpdate_TblLast" runat="server" Text="Update" class="btn btn-outline btn-warning" Style="float: right; margin-right: 15px;" OnClick="btnUpdate_TblLast_Click" />
            <asp:Button ID="btnInsert_TblLast" runat="server" Text="Insert" class="btn btn-outline btn-success" Style="float: right; margin-right: 15px;" OnClick="btnInsert_TblLast_Click" />
        </div>
       
       <div id="tbl_IUlast" runat="server">
                <table style="width: 100%; margin: 20px; margin-left: 70px;">
                    <tr>
                        <td class="auto-style2">
                            <asp:RequiredFieldValidator ID="vld_txt_IDshowLast" runat="server" ControlToValidate="txt_IDshowLast" Text="*" ForeColor="Red" ValidationGroup="vldGroup_tblLast"></asp:RequiredFieldValidator>
                            <b>IDshow:</b>
                        </td>
                        <td class="auto-style1">
                            <asp:TextBox ID="txt_IDshowLast" type="Number" runat="server" Width="200px" Style="margin-bottom: 5px;"></asp:TextBox>
                        </td>
                        <td class="auto-style3">
                            <asp:RequiredFieldValidator ID="vld_txt_PerformerLast" runat="server" ControlToValidate="txt_PerformerLast" Text="*" ForeColor="Red" ValidationGroup="vldGroup_tblLast"></asp:RequiredFieldValidator>
                            <b>Performer:</b>
                        </td>
                        <td class="auto-style7">
                            <asp:TextBox ID="txt_PerformerLast" runat="server" Width="200px" Style="margin-bottom: 5px;"></asp:TextBox>
                        </td>
                        <td class="auto-style3"><b>&nbsp; &nbsp;Image:</b></td>
                        <td>
                            <asp:TextBox ID="txt_ImageLast" runat="server" Width="200px" Style="margin-bottom: 5px;"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style3">
                            <asp:RequiredFieldValidator ID="vld_txt_CityLast" runat="server" ControlToValidate="txt_CityLast" Text="*" ForeColor="Red" ValidationGroup="vldGroup_tblLast"></asp:RequiredFieldValidator>
                            <b>City:</b>
                        </td>
                        <td class="auto-style7">
                            <asp:TextBox ID="txt_CityLast" runat="server" Width="200px" Style="margin-bottom: 5px;"></asp:TextBox>
                        </td>
                        <td class="auto-style3">
                            <asp:RequiredFieldValidator ID="vld_txt_CountryLast" runat="server" ControlToValidate="txt_CountryLast" Text="*" ForeColor="Red" ValidationGroup="vldGroup_tblLast"></asp:RequiredFieldValidator>
                            <b>Country:</b>
                        </td>
                        <td>
                            <asp:TextBox ID="txt_CountryLast" runat="server" Width="200px" Style="margin-bottom: 5px;"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style3">                           
                            <b>&nbsp;&nbsp;YouTube:</b>
                        </td>
                        <td class="auto-style6">
                            <asp:TextBox ID="txt_YouTubeLinq" runat="server" Width="200px" Style="margin-bottom: 5px;"></asp:TextBox>
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>
                            <asp:Button ID="btn_CancelLast" runat="server" Text="Cancel" class="btn btn-outline btn-default" Style="margin-left: 60px;" OnClick="btn_CancelLast_Click" />
                            <asp:Button ID="btn_InsertToDBlast" runat="server" Text="Insert" class="btn btn-outline btn-success" Style="margin-left: 5px;" ValidationGroup="vldGroup_tblLast" OnClick="btn_InsertToDBlast_Click" />
                            <asp:Button ID="btn_UpdateToDBlast" runat="server" Text="Update" class="btn btn-outline btn-warning" ValidationGroup="vldGroup_tblLast" OnClick="btn_UpdateToDBlast_Click"/>
                        </td>
                    </tr>
                </table>
            </div>
             
     

    </div>
    <%--END Table_LastShows--%>






        <%--Chart Pie Order--%>
        <div style="margin-bottom: 50px; margin-top:-10px;">
            <div style="float: left; margin: 0 auto; margin-left: 170px;">
                <asp:Chart ID="ChartPieOrder" runat="server" Height="500px" Width="350px" Palette="SemiTransparent" Style="margin-right: 50px;">
                    <Series>
                        <asp:Series Name="Series" XValueMember="Performer" YValueMembers="NumberOfTickets" ChartType="Pie" YValuesPerPoint="4"></asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="ChartArea"></asp:ChartArea>
                    </ChartAreas>
                </asp:Chart>
            </div>

            <%--Chart Pie Price--%>
            <div style="margin: 0 auto;">
                <asp:Chart ID="ChartPiePrice" runat="server" Height="500px" Width="400px" Palette="SemiTransparent">
                    <Series>
                        <asp:Series Name="Series" XValueMember="Performer" YValueMembers="Price" ChartType="Doughnut"></asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="ChartArea"></asp:ChartArea>
                    </ChartAreas>
                </asp:Chart>
            </div>
            <div style="text-align: center;">
                <asp:Label ID="lblTotalTickets" runat="server" Text="0" Style="margin-right: 80px; font-size: 2em; font-weight: bold; color: darkorange"></asp:Label>
                <asp:Label ID="lblChartTotalPrice" runat="server" Text="0" Style="margin-left: 80px; font-size: 2em; font-weight: bold; color: darkorange"></asp:Label>
            </div>
        </div>
    
    <hr />
    <br />
    <h3>Total users that sign-up per month: (2016)</h3>
    <%--Chart Total Users Per Month--%>
    <asp:Chart ID="ChartTotalUsersPerMonth" runat="server" Height="150px" Width="500px">
        <Series>
            <asp:Series Name="Series" XValueMember="MonthName" YValueMembers="CountUsers"></asp:Series>
        </Series>
        <ChartAreas>
            <asp:ChartArea Name="ChartArea"></asp:ChartArea>
        </ChartAreas>
    </asp:Chart>
        <asp:Label ID="lblTotalSignUsers2016" runat="server" Text="" Style="font-size: 1.5em; font-weight: bold;"></asp:Label>
    <div>
        <br />
        <br />
        <hr />
        <asp:Label ID="lblTotalSignUsers" runat="server" Text="" Style="font-size: 2em; font-weight: bold; color: darkorange"></asp:Label>
             <br />
        <hr />
    </div>
</asp:Content>
