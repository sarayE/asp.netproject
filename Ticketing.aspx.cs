﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using Ticket4MeDAL;
using System.Drawing;

public partial class Ticketing : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //Image:
            (Master as MyMasterStyle).ImageTitle = "Images/TicketShowscrowd.jpg";

            Paid.Visible = false;
            lblComplet.Visible = false;

            string CS = ConfigurationManager.ConnectionStrings["DBconnShows"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(CS))
            {
                string sql = "SELECT Performer, Place, City, Country, Day, Date, Time, TicketPrice, IDshow FROM Table_AllNextShow ORDER BY Performer";
                SqlCommand comnd = new SqlCommand(sql, conn);

                conn.Open();

                using (SqlDataReader rdr = comnd.ExecuteReader())
                {
                    DataTable tbl = new DataTable();

                    //Add a columns to the 
                    tbl.Columns.Add("Performer");
                    tbl.Columns.Add("Place");
                    tbl.Columns.Add("City");
                    tbl.Columns.Add("Country");
                    tbl.Columns.Add("Day");
                    tbl.Columns.Add("Date");
                    tbl.Columns.Add("Time");
                    tbl.Columns.Add("Price");
                    tbl.Columns.Add("IDshow");

                    while (rdr.Read())
                    {
                        DataRow row = tbl.NewRow();

                        //save the sql-parameters 
                        string Performer = Convert.ToString(rdr["Performer"]);
                        string Place = Convert.ToString(rdr["Place"]);
                        string City = Convert.ToString(rdr["City"]);
                        string Country = Convert.ToString(rdr["Country"]);
                        string Day = Convert.ToString(rdr["Day"]);
                        DateTime Date = Convert.ToDateTime(rdr["Date"]);
                        TimeSpan Time = (TimeSpan)(rdr["Time"]);
                        decimal TicketPrice = Convert.ToDecimal(rdr["TicketPrice"]);
                        int IDshow = Convert.ToInt32(rdr["IDshow"]);

                        //insert the sql-parameters into a row order by they columns
                        row["Performer"] = Performer;
                        row["Place"] = Place;
                        row["City"] = City;
                        row["Country"] = Country;
                        row["Day"] = Day;
                        row["Date"] = Date.ToShortDateString();
                        row["Time"] = Time.ToString(@"hh\:mm");
                        row["Price"] = string.Format("{0:#,#.##}$", TicketPrice);
                        row["IDshow"] = IDshow;
                        
                        tbl.Rows.Add(row);
                        gvNextShows.DataSource = tbl;
                        gvNextShows.DataBind();
                    }
                    foreach (GridViewRow row in gvNextShows.Rows)
                    {
                        row.Cells[1].Enabled = false;
                        if (Session["userFN"] == null)
                        {
                            row.Cells[0].Enabled = false;
                        }
                        else
                        {
                            row.Cells[0].Enabled = true;
                            btnSignUpFirst.Visible = false;
                        }
                    }
                }
            }
        }
    }

    protected void ChkChooseShows_CheckedChanged(object sender, EventArgs e)
    {
        string ttlPrice = "";
        lblTotalPrice.Text = "";
        decimal totalPrice = 0;
        decimal ticketPrice;

        foreach (GridViewRow row in gvNextShows.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                row.Cells[1].Enabled = false;
                CheckBox chkRow = (row.Cells[0].FindControl("ChkChooseShows") as CheckBox);
                TextBox txtNumberOfTickets = (row.Cells[0].FindControl("txtNumberOfTickets") as TextBox);
                txtNumberOfTickets.ForeColor = Color.Black;
                row.Font.Bold = false;

                if (chkRow.Checked == true)
                {
                    row.Cells[1].Enabled = true;
                    txtNumberOfTickets.Enabled = true;

                    string price = row.Cells[9].Text;
                    ttlPrice = (Before(price, "$"));
                    ticketPrice = Convert.ToDecimal(ttlPrice);

                    if (string.IsNullOrEmpty(txtNumberOfTickets.Text))
                    {
                        txtNumberOfTickets.Text = "0";
                        txtNumberOfTickets.ForeColor = Color.Red;
                    }
                    else if ((Convert.ToInt32(txtNumberOfTickets.Text) <= 0))
                    {
                        txtNumberOfTickets.ForeColor = Color.Red;
                    }
                    else
                    {
                        row.Font.Bold = true;
                        int ticketsNum = Convert.ToInt32(txtNumberOfTickets.Text);
                        totalPrice = totalPrice + (ticketPrice * ticketsNum);
                        lblTotalPrice.Text = string.Format("<b>Total Price: {0:#,#.##}$<b/>", totalPrice);

                        Paid.Visible = true;
                        lblComplet.Visible = false;

                        if (Session["userFN"] != null)
                        {
                            txtFN.Text = Session["userFN"].ToString();
                            txtLN.Text = Session["userLN"].ToString();
                            txtEmail.Text = Session["userEmail"].ToString();
                            lblComplet.Visible = false;
                        }
                        else
                        {
                            txtFN.Text = "";
                            txtLN.Text = "";
                            txtEmail.Text = "";
                            lblComplet.Visible = false;
                        }
                    }
                }
                else if (totalPrice == 0)
                {
                    Paid.Visible = false;
                    lblComplet.Visible = false;
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        txtNumberOfTickets = (row.Cells[0].FindControl("txtNumberOfTickets") as TextBox);
                    }
                }
            }
        }
    }

    public string Before(string value, string a)
    {
        int posA = value.IndexOf(a);
        if (posA == -1)
        {
            return "";
        }
        return value.Substring(0, posA);
    }

    protected void btnBuy_Click(object sender, EventArgs e)
    {
        int error = 0;
        foreach (GridViewRow row in gvNextShows.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                TextBox txtNumberOfTickets = (row.Cells[0].FindControl("txtNumberOfTickets") as TextBox);
                if (txtNumberOfTickets.ForeColor == Color.Red)
                {
                    error++;
                }

                if (error == 0)
                {
                    //Take it to the database:

                    string CS = ConfigurationManager.ConnectionStrings["DBconnShows"].ConnectionString;
                    using (SqlConnection conn = new SqlConnection(CS))
                    {
                        string sql = "SELECT UserID FROM Table_Users WHERE Email=@Email AND Password=@Password";

                        using (SqlCommand cmd = new SqlCommand())
                        {
                            cmd.Connection = conn;
                            cmd.CommandText = sql;
                            cmd.Parameters.AddWithValue("@Email", Session["userEmail"].ToString());
                            cmd.Parameters.AddWithValue("@Password", Session["userPasswrd"].ToString());
                            conn.Open();
                            var objUserID = (cmd.ExecuteScalar());
                            int userID = int.Parse(objUserID.ToString());

                            CheckBox chkRow = (row.Cells[0].FindControl("ChkChooseShows") as CheckBox);
                            if (chkRow.Checked == true)
                            {
                                sql = "INSERT INTO Table_TicketsOrdered (UserID, IDshow, NumberOfTickets, OrderedTime) VALUES (@UserID, @IDshow, @NumberOfTickets, @OrderedTime)";
                                cmd.CommandText = sql;
                                cmd.Parameters.AddWithValue("@UserID", userID);
                                cmd.Parameters.AddWithValue("@IDshow", Convert.ToInt32(row.Cells[10].Text));
                                cmd.Parameters.AddWithValue("@NumberOfTickets", Convert.ToInt32(txtNumberOfTickets.Text));
                                cmd.Parameters.AddWithValue("@OrderedTime", DateTime.Now);
                            }
                            cmd.ExecuteNonQuery();
                        }
                    }

                    //////////////////////////////

                    txtCredit.Text = "";
                    Paid.Visible = false;
                    lblComplet.Visible = true;
                    row.Font.Bold = false;

                    CheckBox chkRow1 = (row.Cells[0].FindControl("ChkChooseShows") as CheckBox);
                    if (chkRow1.Checked == true)
                    {
                        chkRow1.Checked = false;
                    }

                    if (txtNumberOfTickets.Enabled == true)
                    {
                        txtNumberOfTickets.Text = "1";
                        txtNumberOfTickets.Enabled = false;
                    }
                }
            }
        }
    }
           
    protected void btnSignUpFirst_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/NewLogin.aspx");
    }


    //Hide the "IDshow" column
    protected void gvNextShows_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        e.Row.Cells[10].Visible = false;
    }
}
