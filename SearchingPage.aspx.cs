﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

public partial class SearchingPage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //Image:
            (Master as MyMasterStyle).ImageTitle = "Images/SearchingShowscrowd.jpg";
        }

        string CS = ConfigurationManager.ConnectionStrings["DBconnShows"].ConnectionString;
        using (SqlConnection conn = new SqlConnection(CS))
        {
            SqlCommand comnd = new SqlCommand();
            comnd.Connection = conn;
            //------------------------------------------------------------------//

            //Next shows result:
            string sql = "SELECT Performer, Image, Place, City, Country, Day, Date, Time FROM Table_AllNextShow WHERE Performer LIKE @Searching ORDER BY Date";
            comnd.CommandText = sql;
      
            comnd.Parameters.AddWithValue("@Searching", "%" + Session["UserShearch"].ToString() + "%");
            conn.Open();

            using (SqlDataReader rdr = comnd.ExecuteReader())
            {
                TableRow Row = null;
                for (int i = 0; rdr.Read(); i++)
                {
                    string Performer = Convert.ToString(rdr["Performer"]);
                    string Image = Convert.ToString(rdr["Image"]);
                    string Place = Convert.ToString(rdr["Place"]);
                    string City = Convert.ToString(rdr["City"]);
                    string Country = Convert.ToString(rdr["Country"]);
                    string Day = Convert.ToString(rdr["Day"]);
                    DateTime Date = Convert.ToDateTime(rdr["Date"]);
                    TimeSpan Time = (TimeSpan)(rdr["Time"]);

                    if (Performer != null)
                    {
                        //ucNextShow:
                        UC_Show myShow = (UC_Show)LoadControl("~/UC/ucNextShow.ascx");

                        myShow.NextShowTitle = Performer;
                        myShow.NextShowImage = Image;
                        myShow.NextShowDetails = string.Format("<b>{0}</b> {1}, {2} <br/> {3} <br/> {4} <br/> {5}:{6} PM", Place, City, Country, Day, Date.ToShortDateString(), Time.Hours, Time.Minutes);

                        TableCell Cell = new TableCell();
                        Cell.Controls.Add(myShow);

                        if (i % 4 == 0)
                        {
                            Row = new TableRow();
                            TblNextShow.Rows.Add(Row);
                        }

                        Row.Cells.Add(Cell);
                    }
                    else
                    {
                        lblResuleNext.Text = "No resolt found for '" + Session["UserShearch"].ToString() + "' next shows";
                    }
                }
                if (TblNextShow.Rows.Count <= 0)
                {
                    lblResuleNext.Text = "No resolt found for '" + Session["UserShearch"].ToString() + "' next shows";
                }
            }
            comnd.Parameters.Clear();

            //Last shows result:
            sql = "SELECT Performer, Image, City, Country, YouTubeLinq FROM Table_LastShows WHERE Performer LIKE @Searching ORDER BY Performer";
            comnd.CommandText = sql;

            comnd.Parameters.AddWithValue("@Searching", "%" + Session["UserShearch"].ToString() + "%");

            using (SqlDataReader rdr = comnd.ExecuteReader())
            {
                TableRow Row = null;
                for (int i = 0; rdr.Read(); i++)
                {
                    string Performer = Convert.ToString(rdr["Performer"]);
                    string Image = Convert.ToString(rdr["Image"]);
                    string City = Convert.ToString(rdr["City"]);
                    string Country = Convert.ToString(rdr["Country"]);
                    string YouTubeLinq = Convert.ToString(rdr["YouTubeLinq"]);

                    if (Performer != null)
                    {
                        //ucNextShow:
                        UC_ucLastShow myLastShow = (UC_ucLastShow)LoadControl("~/UC/ucLastShow.ascx");

                        myLastShow.LastShowTitle = Performer;
                        myLastShow.LastShowImage = Image;
                        myLastShow.LastShowDetails = string.Format("<b>{0}</b> <br/> {1}", City, Country);
                        myLastShow.LastShowlinkToYouTube = YouTubeLinq;

                        TableCell Cell = new TableCell();
                        Cell.Controls.Add(myLastShow);

                        if (i % 4 == 0)
                        {
                            Row = new TableRow();
                            TblLastShow.Rows.Add(Row);
                        }

                        Row.Cells.Add(Cell);
                    }
                    else
                    {
                        lblResulePast.Text = "No resolt found for '" + Session["UserShearch"].ToString() + "' last shows";
                    }
                }
                if (TblLastShow.Rows.Count <= 0)
                {
                    lblResulePast.Text = "No resolt found for '" + Session["UserShearch"].ToString() + "' last shows";
                }
            }
        }
    }
}