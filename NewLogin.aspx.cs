﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Drawing;
using System.Web.UI.HtmlControls;

public partial class SignIn : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            (Master as MyMasterStyle).ImageTitle = null;
        }
    }

    protected void btnSignup_Click(object sender, EventArgs e)
    {
        txtEmail.ForeColor = Color.Black;

        if (IsValid)
        {
            Insert_Table_Users_FromDB();
        }
    }

    private void Insert_Table_Users_FromDB()
    {
        string CS = ConfigurationManager.ConnectionStrings["DBconnShows"].ConnectionString;
        using (SqlConnection conn = new SqlConnection(CS))
        {
            SqlCommand comnd = new SqlCommand();
            comnd.Connection = conn;

            string myWish = "INSERT INTO Table_Users (Email, [First Name], [Last Name], Password, SignUpTime) VALUES (@Email, @FN, @LN, @Password, @SignUpTime)";
            comnd.CommandText = myWish;

            comnd.Parameters.AddWithValue("@Email", txtEmail.Text);
            comnd.Parameters.AddWithValue("@FN", txtFN.Text);
            comnd.Parameters.AddWithValue("@LN", txtLN.Text);
            comnd.Parameters.AddWithValue("@Password", txtPass.Text);
            comnd.Parameters.AddWithValue("@SignUpTime", DateTime.Now);

            conn.Open();
            try
            {
                comnd.ExecuteNonQuery();
                msgBox("Sign successfully!");
                lblEmailAlreadySing.Text = "";

                Session["userEmail"] = txtEmail.Text;
                Session["userFN"] = txtFN.Text;
                Session["userLN"] = txtLN.Text;
                Session["userPasswrd"] = txtPass.Text;

                (Master as MyMasterStyle).BtnHello.ForeColor = Color.Green;
                (Master as MyMasterStyle).BtnHello.Text = "Hello " + txtFN.Text;
                (Master as MyMasterStyle).BtnHello.Style.Add("cursor", "pointer");
                (Master as MyMasterStyle).LiNewLogin = false;
                (Master as MyMasterStyle).LiLogOut = true;

                Response.Redirect("~/HomePage.aspx");
            }
            catch 
            {
                txtEmail.ForeColor = Color.Red;
                lblEmailAlreadySing.Text = "* This Email is already sing, please change it and try again.";
            }        
        }
    }


    public void msgBox(String msg)
    {
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "Message Box", "<script language='javascript'>alert('" + msg + "')</script>");
    }

}