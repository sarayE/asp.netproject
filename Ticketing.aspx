﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MyMasterStyle.master" AutoEventWireup="true" CodeFile="Ticketing.aspx.cs" Inherits="Ticketing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script>
        $(document).ready(function () {
            $('#liHome liNewLogin liAllNextShows liLastShows liContact').removeClass('active');

            $('#liTicketing').addClass('active');
        });
    </script>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div style="margin-top:-80px;">
        <hr />
        <div class="table-responsive">
        <asp:GridView ID="gvNextShows" runat="server" CssClass="table-hover table table-striped table-bordered" OnRowDataBound="gvNextShows_RowDataBound">
            <Columns>
                <asp:TemplateField HeaderText="">
                    <ItemTemplate>
                        &nbsp;&nbsp;<asp:CheckBox ID="ChkChooseShows" runat="server" ValidationGroup="ck" AutoPostBack="true" OnCheckedChanged="ChkChooseShows_CheckedChanged" Checked="False" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:TextBox ID="txtNumberOfTickets" runat="server" type="number" style="width:40px;" ValidationGroup="vld" Text="1" AutoPostBack="true" OnTextChanged="ChkChooseShows_CheckedChanged"></asp:TextBox>             
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
            </div>
        <br/>

        <%--Paid--%>
        <div id="Paid" runat="server">
            <div class="col-xs-2" style="padding-left:0px;">
              <label for="ex1">First Name</label>  <asp:RequiredFieldValidator ID="vldtxtFNEmpty" runat="server" ControlToValidate="txtFN" Text="*" ForeColor="Red" ValidationGroup="vld"></asp:RequiredFieldValidator>
              <asp:TextBox ID="txtFN" class="form-control" type="text" runat="server"></asp:TextBox>     
            </div>

            <div class="col-xs-2" style="padding-left:0px;">
              <label for="ex1">Last Name</label>  <asp:RequiredFieldValidator ID="vldtxtLNEmpty" runat="server" ControlToValidate="txtLN" Text="*" ForeColor="Red" ValidationGroup="vld"></asp:RequiredFieldValidator>   
              <asp:TextBox ID="txtLN" class="form-control" type="text" runat="server"></asp:TextBox>                
            </div>

            <div class="col-xs-3" style="padding-left:0px;">
              <label for="ex2">Email</label> <asp:RequiredFieldValidator ID="vldtxtEmailEmpty" runat="server" ControlToValidate="txtEmail" Text="*" ForeColor="Red" Display="Dynamic" ValidationGroup="vld"></asp:RequiredFieldValidator>
              <asp:TextBox ID="txtEmail" class="form-control" type="text" runat="server"></asp:TextBox>               
                <asp:RegularExpressionValidator ID="vldtxtEmailValid" runat="server" ControlToValidate="txtEmail" Text="* Invalid Email" ForeColor="Red" ValidationGroup="vld" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="Dynamic"></asp:RegularExpressionValidator>       
            </div>

            <div class="col-xs-3" style="padding-left:0px;">
              <label for="ex1">credit card</label> <asp:RequiredFieldValidator ID="vldtxtCreditValid" runat="server" ControlToValidate="txtCredit" Text="*" ForeColor="Red" ValidationGroup="vld"></asp:RequiredFieldValidator>
              <asp:TextBox ID="txtCredit" class="form-control" type="number" runat="server"></asp:TextBox>                
            </div>

            <div style="padding-left:0px; padding-top:0px;">
                <asp:Label ID="lblTotalPrice" runat="server" Text="" style="font-size:1.5em;"></asp:Label>
                &nbsp;&nbsp;<asp:Button ID="btnBuy" CssClass="btn-warning" runat="server" Text="Buy!" style="width:15%" OnClick="btnBuy_Click" ValidationGroup="vld"/>
            </div>
        </div>
         
         <asp:Label ID="lblComplet" runat="server" Text="<br/><b>Order has been completed. <br/>Tickets will be sent to you by mail. <br/>For inquiries: Tel: 03-222-2223 | Email: Ticket4Me@Ticket.Com<b/>" style="font-size:1em; color:green;" Visible="false"></asp:Label>
        <asp:Button ID="btnSignUpFirst" runat="server" Text="Sign In" OnClick="btnSignUpFirst_Click" class="btn btn-info" style="margin:0 auto; width:20%; float:right;"/>

</div>
</asp:Content>

