﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MyMasterStyle.master" AutoEventWireup="true" CodeFile="EditUser.aspx.cs" Inherits="EditUser" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script>
        $(document).ready(function () {
            $('#liHome').removeClass('active');

            $('#liNewLogin').addClass('active');
        });
    </script>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">



    <div style="width: 60%; margin: auto; margin-top: 10px;">
        <div class="panel panel-info" style="height: 100%; border-color: green;">
            <div class="panel-heading" style="border-color: green; background-color: #149414; color: white;">
                <div class="panel-title">Edit your profile</div>
                <div style="float: right; font-size: 90%; position: relative; top: -25px; color: white;">Ticket<span style="font-size: 1.3em;">➃</span>Me</div>
            </div>

            <div class="panel-body">
                <br />
                <!--Email-->
                <div class="form-group">
                    <label for="email" class="col-md-3 control-label">Email</label>
                    <div class="col-md-9">
                         &nbsp;&nbsp;<asp:Label ID="lblEmailAlreadySing" runat="server" ForeColor="Red" style="font-size:0.9em;"></asp:Label>
                        <asp:TextBox ID="txtEmail" runat="server" class="form-control" name="email" placeholder="Email Adress" Style="width: 70%; margin-left: 10px; margin-right: 10px; margin-bottom: 25px; float: left;"></asp:TextBox>
                        <div style="width: 100px; height: 30px; float: left;">
                            <asp:RequiredFieldValidator ID="vldtxtEmailEmpty" runat="server" ControlToValidate="txtEmail" Text="*" ForeColor="Red" Display="Dynamic" ValidationGroup="vld"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="vldtxtEmailValid" runat="server" ControlToValidate="txtEmail" Text="* Invalid Email" ForeColor="Red" ValidationGroup="vld" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="Dynamic"></asp:RegularExpressionValidator>
                        </div>                        
                        &nbsp;
                    </div>
                </div>

                <!--First Name-->
                <div class="form-group">
                    <label for="firstname" class="col-md-3 control-label">First Name</label>
                    <div class="col-md-9">
                        <asp:TextBox ID="txtFN" runat="server" class="form-control" name="firstname" placeholder="First Name" Style="width: 70%; margin-left: 10px; margin-right: 10px; margin-bottom: 25px; float: left;"></asp:TextBox>
                        <div style="width: 22px; height: 30px; float: left;">
                            <asp:RequiredFieldValidator ID="vldtxtFNEmpty" runat="server" ControlToValidate="txtFN" Text="*" ForeColor="Red" ValidationGroup="vld"></asp:RequiredFieldValidator>
                        </div>
                        &nbsp;
                    </div>
                </div>

                <!--Last Name-->
                <div class="form-group">
                    <label for="lastname" class="col-md-3 control-label">Last Name</label>
                    <div class="col-md-9">
                        <asp:TextBox ID="txtLN" runat="server" class="form-control" name="lastname" placeholder="Last Name" Style="width: 70%; margin-left: 10px; margin-right: 10px; margin-bottom: 25px; float: left;"></asp:TextBox>
                        <div style="width: 22px; height: 30px; float: left;">
                            <asp:RequiredFieldValidator ID="vldtxtLNEmpty" runat="server" ControlToValidate="txtLN" Text="*" ForeColor="Red" ValidationGroup="vld"></asp:RequiredFieldValidator>
                        </div>
                        &nbsp;
                    </div>
                </div>


                &nbsp;&nbsp;&nbsp;&nbsp;<asp:LinkButton ID="linkChangePass" runat="server" Style="color: #1696f6; text-decoration: none;" OnClick="linkChangePass_Click"><b>Click here to change your password.</b></asp:LinkButton>
                <br />
                <br />

                <!--Password-->
                <div runat="server" id="paswrd" style="margin-bottom:10px; margin-top:-40px;">
                    <hr/>
                                    <br />
                    <div class="form-group">
                        <label for="password" class="col-md-3 control-label">Old password</label>
                        <div class="col-md-9">
                            &nbsp;&nbsp;<asp:Label ID="lblOldPassUncorrect" runat="server" ForeColor="Red" style="font-size:0.9em;"></asp:Label>
                            <asp:TextBox ID="txtOldPass" type="password" runat="server" class="form-control" placeholder="Old password" Style="width: 70%; margin-left: 10px; margin-right: 10px; margin-bottom: 25px; float: left;"></asp:TextBox>
                            <div style="width: 22px; height: 30px; float: left;">
                                <asp:RequiredFieldValidator ID="vldtxtOldPassEmpty" runat="server" ControlToValidate="txtOldPass" Text="*" ForeColor="Red" ValidationGroup="vld"></asp:RequiredFieldValidator>
                            </div>
                            &nbsp;
                        </div>
                    </div>


                    <div class="form-group">
                        <label for="password" class="col-md-3 control-label">New password</label>
                        <div class="col-md-9">
                            <asp:TextBox ID="txtNewPass" type="password" runat="server" class="form-control" placeholder="New password" Style="width: 70%; margin-left: 10px; margin-right: 10px; margin-bottom: 25px; float: left;"></asp:TextBox>
                            <div style="width: 22px; height: 30px; float: left;">
                                <asp:RequiredFieldValidator ID="vldtxtNewPassEmpty" runat="server" ControlToValidate="txtNewPass" Text="*" ForeColor="Red" ValidationGroup="vld"></asp:RequiredFieldValidator>
                            </div>
                            &nbsp;
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="password" class="col-md-3 control-label">Confirm new password</label>
                        <div class="col-md-9">
                            <asp:TextBox ID="txtPassConfirm" type="password" runat="server" class="form-control" placeholder="New password" Style="width: 70%; margin-left: 10px; margin-right: 10px; margin-bottom: 25px; float: left;"></asp:TextBox>
                            <div style="width: 22px; height: 30px; float: left;">
                                <asp:RequiredFieldValidator ID="vldtxtPassConfirmEmpty" runat="server" ControlToValidate="txtPassConfirm" Text="*" ForeColor="Red" Display="Dynamic" ValidationGroup="vld"></asp:RequiredFieldValidator>
                            </div>
                            <asp:CompareValidator ID="vldComparePass" runat="server" ControlToCompare="txtNewPass" ControlToValidate="txtPassConfirm" Text="* Passwords do not match" ForeColor="Red" Display="Dynamic" ValidationGroup="vld"></asp:CompareValidator>
                            &nbsp;
                        </div>
                        &nbsp;&nbsp;&nbsp;&nbsp;<asp:LinkButton ID="LinkButton1" runat="server" Style="color: #1696f6; text-decoration: none;"><b>Forget the password ?</b></asp:LinkButton>
                    </div>
                </div>
                <div>
                    <!-- Button -->
                    <div class="col-md-offset-3 col-md-9">
                        <asp:Button ID="btnSignup" type="button" runat="server" Text="Save!" class="btn btn-success" Style="width: 70%; margin: 10px; margin-bottom: 20px;" ValidationGroup="vld" OnClick="btnSignup_Click" />
                        <br />
                    </div>
                </div>

            </div>
        </div>
    </div>



</asp:Content>

