﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Data.SqlClient;
using System.Configuration;

public partial class EditUser : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        { 
            paswrd.Visible = false;
            linkChangePass.Visible = true;

            if (Session["userEmail"] != null)
            { 
                txtEmail.Text = Session["userEmail"].ToString();
                txtFN.Text = Session["userFN"].ToString();
                txtLN.Text = Session["userLN"].ToString();
            }       
        }
    }

    protected void linkChangePass_Click(object sender, EventArgs e)
    {
        paswrd.Visible = true;
        linkChangePass.Visible = false;
        //לערוך מסד נתונים 
        // לסדר כפתורים
        // כאשר לוג-אווט יוצא מעמוד הפרופיל
    }

    protected void btnSignup_Click(object sender, EventArgs e)
    {
        txtEmail.ForeColor = Color.Black;

        if (IsValid && (Session["userEmail"] != null))
        { 
            Update_Table_Users_FromDB();        
        }
    }

    private void Update_Table_Users_FromDB()
    {
        string CS = ConfigurationManager.ConnectionStrings["DBconnShows"].ConnectionString;
        using (SqlConnection conn = new SqlConnection(CS))
        {
            SqlCommand comnd = new SqlCommand();
            comnd.Connection = conn;

            comnd.Parameters.AddWithValue("@Email", txtEmail.Text);
            comnd.Parameters.AddWithValue("@FN", txtFN.Text);
            comnd.Parameters.AddWithValue("@LN", txtLN.Text);
            comnd.Parameters.AddWithValue("@Password", txtNewPass.Text);
            comnd.Parameters.AddWithValue("@MyIdentity", Session["userEmail"].ToString());
            string myWish; 

            conn.Open();
            try
            {
                if (String.IsNullOrEmpty(txtOldPass.Text))
                {
                    myWish = "UPDATE Table_Users SET Email=@Email, [First Name]=@FN, [Last Name]=@LN WHERE Email=@MyIdentity;";
                    comnd.CommandText = myWish;
                    comnd.ExecuteNonQuery();
                    msgBox("Edit successfully!");
                    lblEmailAlreadySing.Text = "";

                    Session["userEmail"] = txtEmail.Text;
                    Session["userFN"] = txtFN.Text;
                    Session["userLN"] = txtLN.Text;

                    (Master as MyMasterStyle).BtnHello.ForeColor = Color.Green;
                    (Master as MyMasterStyle).BtnHello.Text = "Hello " + txtFN.Text;
                    (Master as MyMasterStyle).BtnHello.Style.Add("cursor", "pointer");
                    (Master as MyMasterStyle).LiNewLogin = false;
                    (Master as MyMasterStyle).LiLogOut = true;
                }
                else
                {
                    if (txtOldPass.Text == Session["userPasswrd"].ToString())
                    {
                        myWish = "UPDATE Table_Users SET Email=@Email, [First Name]=@FN, [Last Name]=@LN, Password=@Password WHERE Email=@MyIdentity;";
                        comnd.CommandText = myWish;
                        comnd.ExecuteNonQuery();
                        msgBox("Edit successfully!");
                        lblOldPassUncorrect.Text = "";
                        lblEmailAlreadySing.Text = "";

                        Session["userEmail"] = txtEmail.Text;
                        Session["userFN"] = txtFN.Text;
                        Session["userLN"] = txtLN.Text;
                        Session["userPasswrd"] = txtNewPass.Text;

                        (Master as MyMasterStyle).BtnHello.ForeColor = Color.Green;
                        (Master as MyMasterStyle).BtnHello.Text = "Hello " + txtFN.Text;
                        (Master as MyMasterStyle).BtnHello.Style.Add("cursor", "pointer");
                        (Master as MyMasterStyle).LiNewLogin = false;
                        (Master as MyMasterStyle).LiLogOut = true;
                    }
                    else
                    {
                        lblOldPassUncorrect.Text = "* The Old Password is not correct.";
                    }
                }

            }
            catch
            {
                txtEmail.ForeColor = Color.Red;
                lblEmailAlreadySing.Text = "* This Email is already sing, please change it and try again.";
            }
        }
    }


    public void msgBox(String msg)
    {
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "Message Box", "<script language='javascript'>alert('" + msg + "')</script>");
    }
}