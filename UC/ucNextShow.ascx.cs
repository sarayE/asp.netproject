﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UC_Show : System.Web.UI.UserControl
{
    public string NextShowTitle
    {
        get { return lblTitle.Text;  }
        set { lblTitle.Text = value; }
    }

    public string NextShowImage
    {
        get { return imgNextShow.ImageUrl; }
        set { imgNextShow.ImageUrl = value; }
    }

    public string NextShowDetails
    {
        get { return lblDetails.Text; }
        set { lblDetails.Text = value; }
    }

    public string NextShowlinkToBuyTicket
    {
        get { return linkBuyTicket.NavigateUrl; }
        set { linkBuyTicket.NavigateUrl = value; }
    }

}