﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UC_ucLastShow : System.Web.UI.UserControl
{
    public string LastShowTitle
    {
        get { return lblTitle.Text; }
        set { lblTitle.Text = value; }
    }

    public string LastShowImage
    {
        get { return imgLastShow.ImageUrl; }
        set { imgLastShow.ImageUrl = value; }
    }

    public string LastShowDetails
    {
        get { return lblDetails.Text; }
        set { lblDetails.Text = value; }
    }

    public string LastShowlinkToYouTube
    {
        get { return linkToYouTube.NavigateUrl; }
        set { linkToYouTube.NavigateUrl = value; }
    }
}