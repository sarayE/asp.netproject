﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucLastShow.ascx.cs" Inherits="UC_ucLastShow" %>

<%--//Last--%>

<div style="width:289px; height:202px; margin-bottom:90px; text-align:center;" >

    <asp:Label ID="lblTitle" runat="server" style="text-shadow:initial; font-weight:bold; font-size:1.5em; "></asp:Label>
    <br />
    <asp:Image ID="imgLastShow" runat="server" style="border-radius:15px; border:1px solid black; width:100px; height:109px; margin: 0 auto; margin-bottom:10px; margin-top:5px;"/>
    <br />
    <asp:Label ID="lblDetails" runat="server" ></asp:Label>
    <br />
    <asp:HyperLink ID="linkToYouTube" runat="server" class="btn btn-default" style="margin-top:5px; margin-bottom:5px;" Target="_blank">
    Watch on YouTube  <img src="../Images/youtube-icon.png" />
    </asp:HyperLink>

</div>