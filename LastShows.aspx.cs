﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;

public partial class LastShows : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //Image:
            (Master as MyMasterStyle).ImageTitle = "Images/LastShowscrowd.jpg";
        }

        Get_Table_LastShows_FromDB();
    }

    private void Get_Table_LastShows_FromDB()
    {
        string CS = ConfigurationManager.ConnectionStrings["DBconnShows"].ConnectionString;
        using(SqlConnection conn = new SqlConnection(CS))
        {
            SqlCommand comnd = new SqlCommand();
            comnd.Connection = conn;

            string myWish = "SELECT Performer, Image, City, Country, YouTubeLinq FROM Table_LastShows ORDER BY Performer";
            comnd.CommandText = myWish;

            conn.Open();

            using (SqlDataReader ridr = comnd.ExecuteReader())
            {

                TableRow Row = null;
                for (int i = 0; ridr.Read(); i++)
                {
                    string Performer = Convert.ToString(ridr["Performer"]);
                    string Image = Convert.ToString(ridr["Image"]);
                    string City = Convert.ToString(ridr["City"]);
                    string Country = Convert.ToString(ridr["Country"]);
                    string YouTubeLinq = Convert.ToString(ridr["YouTubeLinq"]);


                    if (Performer != null)
                    {
                        //ucNextShow:
                        UC_ucLastShow myLastShow = (UC_ucLastShow)LoadControl("~/UC/ucLastShow.ascx");

                        myLastShow.LastShowTitle = Performer;
                        myLastShow.LastShowImage = Image;
                        myLastShow.LastShowDetails = string.Format("<b>{0}</b> <br/> {1}", City, Country);
                        myLastShow.LastShowlinkToYouTube = YouTubeLinq;

                        TableCell Cell = new TableCell();
                        Cell.Controls.Add(myLastShow);

                        if (i % 4 == 0)
                        {
                            Row = new TableRow();
                            TblLastShow.Rows.Add(Row);
                        }

                        Row.Cells.Add(Cell);
                    }
                } 
            }
        }
    }
}
