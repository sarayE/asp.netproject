﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;
using Ticket4MeDAL;

public partial class Admin : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Cache["AllNextShow&LastShows"] == null)
        {
            string CS = ConfigurationManager.ConnectionStrings["DBconnShows"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(CS))
            {
                string sql = "SELECT * FROM Table_AllNextShow ORDER BY Date;"
                    + "SELECT * FROM Table_LastShows ORDER BY Performer;";

                SqlDataAdapter adptr = new SqlDataAdapter(sql, conn);
                DataSet ds = new DataSet();
                adptr.Fill(ds);

                if (!IsPostBack)
                {
                    tbl_IUnext.Visible = false;
                    tbl_IUlast.Visible = false;

                    ds.Tables[0].TableName = "Table_AllNextShow";
                    gvAllNextShow.DataSource = ds.Tables["Table_AllNextShow"];
                    gvAllNextShow.DataBind();
                    Cache["Table_AllNextShow"] = ds.Tables["Table_AllNextShow"];
                    for (int i = 0; i < gvAllNextShow.Rows.Count; i++)
                    {
                        lblNextshowsPanel.Text = "Total: " + i;
                    }

                    ds.Tables[1].TableName = "Table_LastShows";
                    gvlastShows.DataSource = ds.Tables["Table_LastShows"];
                    gvlastShows.DataBind();
                    Cache["Table_LastShows"] = ds.Tables["Table_LastShows"];
                    for (int i = 0; i < gvlastShows.Rows.Count; i++)
                    {
                        lblLastshowsPanel.Text = "Total: " + i;
                    }
                }

                //Chart total price - pie
                sql = "SELECT Performer, NumberOfTickets FROM Table_AllNextShow INNER JOIN Table_TicketsOrdered ON Table_AllNextShow.IDshow = Table_TicketsOrdered.IDshow";
                adptr = new SqlDataAdapter(sql, conn);
                DataTable dtOrders = new DataTable();
                adptr.Fill(dtOrders);
                ChartPieOrder.DataSource = dtOrders;
                ChartPieOrder.DataBind();
                ChartPieOrder.Series[0].Label = "#VALX \n(#VALY)";

                int sumTickets = 0;
                foreach (DataRow dr in dtOrders.Rows)
                {
                    sumTickets += Convert.ToInt32(dr["NumberOfTickets"]);
                }
                lblTotalTickets.Text = "Total sold tickets : " + sumTickets.ToString();


                //Chart total ticket - pie
                sql = "SELECT Performer, TicketPrice * NumberOfTickets as Price FROM Table_AllNextShow INNER JOIN Table_TicketsOrdered ON Table_AllNextShow.IDshow = Table_TicketsOrdered.IDshow";
                adptr = new SqlDataAdapter(sql, conn);
                DataTable dtOrdersPrice = new DataTable();
                adptr.Fill(dtOrdersPrice);
                ChartPiePrice.DataSource = dtOrdersPrice;
                ChartPiePrice.DataBind();
                ChartPiePrice.Series[0].Label = "#VALX \n(#VALY{#,##} $)";

                int sumPrice = 0;
                foreach (DataRow dr in dtOrdersPrice.Rows)
                {
                    sumPrice += Convert.ToInt32(dr["Price"]);
                }
                lblChartTotalPrice.Text = "Total income: " + sumPrice.ToString("N0") + " $";


                //Chart total ticket - columns
                sql = "Procedure_CountUsersPerMonth";
                adptr = new SqlDataAdapter(sql, conn);
                adptr.SelectCommand.CommandType = CommandType.StoredProcedure;
                DataTable tblUsersMonth = new DataTable();
                adptr.Fill(tblUsersMonth);
                ChartTotalUsersPerMonth.DataSource = tblUsersMonth;
                ChartTotalUsersPerMonth.DataBind();

                //////////////////////////////////////////

                //total sign-up users:
                sql = "select COUNT(SignUpTime) AS totalUsers FROM Table_Users";
                SqlCommand comnd = new SqlCommand(sql, conn);
                conn.Open();
                int totalUsersCount = 0;
                using (SqlDataReader rdr = comnd.ExecuteReader())
                {
                    while (rdr.Read())
                    {
                        //save the sql-parameters 
                        int totalUsers = Convert.ToInt32(rdr["totalUsers"]);
                        totalUsersCount = totalUsersCount + totalUsers;
                    }
                }
                lblTotalSignUsers.Text = "Total sign: " + totalUsersCount.ToString();
                //total sign-up users for 2016:
                sql = "select COUNT(SignUpTime) AS totalUsers2016 FROM Table_Users WHERE YEAR(SignUpTime)=2016";
                comnd = new SqlCommand(sql, conn);
                int totalUsersCount2016 = 0;
                using (SqlDataReader rdr = comnd.ExecuteReader())
                {
                    while (rdr.Read())
                    {
                        //save the sql-parameters 
                        int totalUsers2016 = Convert.ToInt32(rdr["totalUsers2016"]);
                        totalUsersCount2016 = totalUsersCount2016 + totalUsers2016;
                    }
                }
                lblTotalSignUsers2016.Text = "= " + totalUsersCount2016.ToString();
            }
        }
        else
        {
            gvAllNextShow.DataSource = (DataSet)Cache["Table_AllNextShow"];
            gvAllNextShow.DataBind();

            gvlastShows.DataSource = (DataSet)Cache["Table_LastShows"];
            gvlastShows.DataBind();
        }
    }

    //Tbl-Next:

    //-Delete(Next)
    protected void btnDel_TblNext_Click(object sender, EventArgs e)
    {
        int chkBoxToDelete = 0;
        string CS = ConfigurationManager.ConnectionStrings["DBconnShows"].ConnectionString;
        foreach (GridViewRow row in gvAllNextShow.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                CheckBox chkRow = (row.Cells[0].FindControl("ChkChooseShows_TblNext") as CheckBox);
                if (chkRow.Checked == true)
                {
                    chkBoxToDelete++;
                    int IDshow = Convert.ToInt32(gvAllNextShow.Rows[row.RowIndex].Cells[1].Text);

                    //Delete//
                    using (SqlConnection conn = new SqlConnection(CS))
                    {
                        string sql = "DELETE FROM Table_AllNextShow WHERE IDshow=@IDshow";
                        using (SqlCommand cmd = new SqlCommand(sql, conn))
                        {
                            SqlParameter prm = new SqlParameter();
                            prm.ParameterName = "@IDshow";
                            prm.Value = IDshow;
                            prm.SqlDbType = System.Data.SqlDbType.Int;
                            cmd.Parameters.Add(prm);
                            conn.Open();
                            try
                            {
                                cmd.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                throw new Exception("Delete wad failed: " + ex.Message);
                            }
                        }
                    }
                }
            }
        }
        if (chkBoxToDelete == 0)
        {
            msgBox("Choose an items to delete.");
        }
        else
        {
            msgBox("Delete successfully!");
            string sql1 = "SELECT * FROM Table_AllNextShow ORDER BY Date;";
            using (SqlConnection conn = new SqlConnection(CS))
            {
                SqlDataAdapter adptr = new SqlDataAdapter(sql1, conn);
                DataTable dt = new DataTable();
                adptr.Fill(dt);
                gvAllNextShow.DataSource = null;
                gvAllNextShow.DataSource = dt;
                gvAllNextShow.DataBind();
                for (int i = 0; i < gvAllNextShow.Rows.Count; i++)
                {
                    lblNextshowsPanel.Text = "Total: " + i;
                }
                Cache["Table_AllNextShow"] = dt;
            }
        }
    }

    //-Insert(Next)
    protected void btnInsert_TblNext_Click(object sender, EventArgs e)
    {
        btnInsert_TblNext.Visible = false;
        btnUpdate_TblNext.Visible = false;
        btnDel_TblNext.Visible = false;
        tbl_IUnext.Visible = true;
        btn_UpdateToDBnext.Visible = false;
        btn_InsertToDBnext.Visible = true;

        txt_IDshowNext.Enabled = false;

        foreach (GridViewRow row in gvAllNextShow.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                CheckBox chkRow = (row.Cells[0].FindControl("ChkChooseShows_TblNext") as CheckBox);
                chkRow.Enabled = false;
                if (chkRow.Checked == true)
                {
                    chkRow.Checked = false;
                }
            }
        }
    }
    protected void btn_InsertToDBnext_Click(object sender, EventArgs e)
    {
        bool IsparsTime = false;
        bool IsparsDate = false;
        txt_DateNextError.Text = "";
        txt_TimeNextError.Text = "";
        if (IsValid)
        {
            //Insert the items to the database:
            string CS = ConfigurationManager.ConnectionStrings["DBconnShows"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(CS))
            {
                string sql = "INSERT INTO Table_AllNextShow (Performer, Image, Place, City, Country, Day, Date, Time, TicketPrice) VALUES (@Performer, @Image, @Place, @City, @Country, @Day, @Date, @Time, @TicketPrice)";
                using (SqlCommand cmd = new SqlCommand(sql, conn))
                {
                    //Defind the parametrs:

                    SqlParameter prm = new SqlParameter();
                    prm.ParameterName = "@Performer";
                    prm.Value = txt_PerformerNext.Text;
                    prm.SqlDbType = System.Data.SqlDbType.NVarChar;
                    prm.Size = 50;
                    cmd.Parameters.Add(prm);

                    prm = new SqlParameter();
                    prm.ParameterName = "@Image";
                    prm.Value = txt_ImageNext.Text;
                    prm.SqlDbType = System.Data.SqlDbType.NVarChar;
                    cmd.Parameters.Add(prm);

                    prm = new SqlParameter();
                    prm.ParameterName = "@Place";
                    prm.Value = txt_PlaceNext.Text;
                    prm.SqlDbType = System.Data.SqlDbType.NVarChar;
                    prm.Size = 50;
                    cmd.Parameters.Add(prm);

                    prm = new SqlParameter();
                    prm.ParameterName = "@City";
                    prm.Value = txt_CityNext.Text;
                    prm.SqlDbType = System.Data.SqlDbType.NVarChar;
                    prm.Size = 50;
                    cmd.Parameters.Add(prm);

                    prm = new SqlParameter();
                    prm.ParameterName = "@Country";
                    prm.Value = txt_CountryNext.Text;
                    prm.SqlDbType = System.Data.SqlDbType.NVarChar;
                    prm.Size = 50;
                    cmd.Parameters.Add(prm);

                    prm = new SqlParameter();
                    prm.ParameterName = "@Day";
                    prm.Value = dd_DayNext.SelectedItem.Text;
                    prm.SqlDbType = System.Data.SqlDbType.NVarChar;
                    prm.Size = 15;
                    cmd.Parameters.Add(prm);

                    prm = new SqlParameter();
                    prm.ParameterName = "@Date";
                    DateTime parsDate = new DateTime();
                    IsparsDate = DateTime.TryParse(txt_DateNext.Text, out parsDate);
                    if (IsparsDate == true)
                    {
                        prm.Value = parsDate;
                        prm.SqlDbType = System.Data.SqlDbType.Date;
                        cmd.Parameters.Add(prm);
                    }
                    else
                    {
                        txt_DateNextError.Text = "* dd/mm/yyyy";
                    }

                    prm = new SqlParameter();
                    prm.ParameterName = "@Time";
                    TimeSpan parsTime = new TimeSpan();
                    IsparsTime = TimeSpan.TryParse(txt_TimeNext.Text, out parsTime);
                    if (IsparsTime == true)
                    {
                        prm.Value = parsTime;
                        prm.SqlDbType = System.Data.SqlDbType.Time;
                        cmd.Parameters.Add(prm);
                    }
                    else
                    {
                        txt_TimeNextError.Text = "* hh:mm";
                    }

                    prm = new SqlParameter();
                    prm.ParameterName = "@TicketPrice";
                    prm.Value = Convert.ToDecimal(txt_TicketPrice.Text);
                    prm.SqlDbType = System.Data.SqlDbType.Money;
                    cmd.Parameters.Add(prm);

                    if (IsparsTime == true && IsparsDate == true)
                    {
                        try
                        {
                            conn.Open();
                            cmd.ExecuteNonQuery();
                            msgBox("Insert successfully!");
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Insert operation was failes." + ex.Message);
                        }
                    }
                }
            }
            if (IsparsTime == true && IsparsDate == true)
            {
                tbl_IUnext.Visible = false;
                btnInsert_TblNext.Visible = true;
                btnUpdate_TblNext.Visible = true;
                btnDel_TblNext.Visible = true;

                txt_IDshowNext.Text = "";
                txt_PerformerNext.Text = "";
                txt_ImageNext.Text = "";
                txt_PlaceNext.Text = "";
                txt_CityNext.Text = "";
                txt_CountryNext.Text = "";
                dd_DayNext.ClearSelection();
                txt_DateNext.Text = "";
                txt_TimeNext.Text = "";
                txt_TicketPrice.Text = "";

                string sql1 = "SELECT * FROM Table_AllNextShow ORDER BY Date;";
                using (SqlConnection conn = new SqlConnection(CS))
                {
                    SqlDataAdapter adptr = new SqlDataAdapter(sql1, conn);
                    DataTable dt = new DataTable();
                    adptr.Fill(dt);
                    gvAllNextShow.DataSource = null;
                    gvAllNextShow.DataSource = dt;
                    gvAllNextShow.DataBind();
                    for (int i = 0; i < gvAllNextShow.Rows.Count; i++)
                    {
                        lblNextshowsPanel.Text = "Total: " + i;
                    }
                    Cache["Table_AllNextShow"] = dt;
                }
            }
        }
    }

    //-Cancel(Next)
    protected void btn_Cancel_Click(object sender, EventArgs e)
    {
        tbl_IUnext.Visible = false;
        btnInsert_TblNext.Visible = true;
        btnUpdate_TblNext.Visible = true;
        btnDel_TblNext.Visible = true;

        txt_DateNextError.Text = "";
        txt_TimeNextError.Text = "";
        txt_IDshowNext.Text = "";
        txt_PerformerNext.Text = "";
        txt_ImageNext.Text = "";
        txt_PlaceNext.Text = "";
        txt_CityNext.Text = "";
        txt_CountryNext.Text = "";
        dd_DayNext.ClearSelection();
        txt_DateNext.Text = "";
        txt_TimeNext.Text = "";
        txt_TicketPrice.Text = "";

        foreach (GridViewRow row in gvAllNextShow.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                CheckBox chkRow = (row.Cells[0].FindControl("ChkChooseShows_TblNext") as CheckBox);
                chkRow.Enabled = true;
            }
        }
    }

    //-Update(Next)
    protected void btnUpdate_TblNext_Click(object sender, EventArgs e)
    {
        int totalSelected = 0;
        CheckBox chkRow;
        foreach (GridViewRow row in gvAllNextShow.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                chkRow = (row.Cells[0].FindControl("ChkChooseShows_TblNext") as CheckBox);
                if (chkRow.Checked == true)
                {
                    totalSelected++;
                }
            }
        }
        if (totalSelected == 1)
        {
            foreach (GridViewRow row in gvAllNextShow.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    chkRow = (row.Cells[0].FindControl("ChkChooseShows_TblNext") as CheckBox);
                    chkRow.Enabled = false;
                    if (chkRow.Checked == true)
                    {
                        txt_IDshowNext.Text = row.Cells[1].Text;
                        txt_PerformerNext.Text = row.Cells[2].Text;
                        txt_ImageNext.Text = row.Cells[3].Text;
                        txt_PlaceNext.Text = row.Cells[4].Text;
                        txt_CityNext.Text = row.Cells[5].Text;
                        txt_CountryNext.Text = row.Cells[6].Text;
                        string day = row.Cells[7].Text;
                        txt_DateNext.Text = row.Cells[8].Text;
                        txt_TimeNext.Text = row.Cells[9].Text;
                        txt_TicketPrice.Text = row.Cells[10].Text;

                        switch (day)
                        {
                            case "Sunday":
                                dd_DayNext.SelectedIndex = 1;
                                break;
                            case "Monday":
                                dd_DayNext.SelectedIndex = 2;
                                break;
                            case "Tuesday":
                                dd_DayNext.SelectedIndex = 3;
                                break;
                            case "Wednesday":
                                dd_DayNext.SelectedIndex = 4;
                                break;
                            case "Thursday":
                                dd_DayNext.SelectedIndex = 5;
                                break;
                            case "Friday":
                                dd_DayNext.SelectedIndex = 6;
                                break;
                            case "Saturday":
                                dd_DayNext.SelectedIndex = 7;
                                break;
                        }
                    }
                }
            }
            btnInsert_TblNext.Visible = false;
            btnUpdate_TblNext.Visible = false;
            btnDel_TblNext.Visible = false;
            tbl_IUnext.Visible = true;
            btn_UpdateToDBnext.Visible = true;
            btn_InsertToDBnext.Visible = false;

            txt_IDshowNext.Enabled = false;
        }
        else
        {
            msgBox("Please choose one item to update.");
        }

    }
    protected void btn_UpdateToDBnext_Click(object sender, EventArgs e)
    {
        bool IsparsTime = false;
        bool IsparsDate = false;
        txt_DateNextError.Text = "";
        txt_TimeNextError.Text = "";
        if (IsValid)
        {
            string CS = ConfigurationManager.ConnectionStrings["DBconnShows"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(CS))
            {
                string sql = "UPDATE Table_AllNextShow SET Performer=@Performer, Image=@Image, Place=@Place, City=@City, Country=@Country, Day=@Day, Date=@Date, Time=@Time, TicketPrice=@TicketPrice WHERE IDshow=@IDshow";
                using (SqlCommand cmd = new SqlCommand(sql, conn))
                {
                    //Defind the parametrs:

                    SqlParameter prm = new SqlParameter();
                    prm.ParameterName = "@Performer";
                    prm.Value = txt_PerformerNext.Text;
                    prm.SqlDbType = System.Data.SqlDbType.NVarChar;
                    prm.Size = 50;
                    cmd.Parameters.Add(prm);

                    prm = new SqlParameter();
                    prm.ParameterName = "@Image";
                    prm.Value = txt_ImageNext.Text;
                    prm.SqlDbType = System.Data.SqlDbType.NVarChar;
                    cmd.Parameters.Add(prm);

                    prm = new SqlParameter();
                    prm.ParameterName = "@Place";
                    prm.Value = txt_PlaceNext.Text;
                    prm.SqlDbType = System.Data.SqlDbType.NVarChar;
                    prm.Size = 50;
                    cmd.Parameters.Add(prm);

                    prm = new SqlParameter();
                    prm.ParameterName = "@City";
                    prm.Value = txt_CityNext.Text;
                    prm.SqlDbType = System.Data.SqlDbType.NVarChar;
                    prm.Size = 50;
                    cmd.Parameters.Add(prm);

                    prm = new SqlParameter();
                    prm.ParameterName = "@Country";
                    prm.Value = txt_CountryNext.Text;
                    prm.SqlDbType = System.Data.SqlDbType.NVarChar;
                    prm.Size = 50;
                    cmd.Parameters.Add(prm);

                    prm = new SqlParameter();
                    prm.ParameterName = "@Day";
                    prm.Value = dd_DayNext.SelectedItem.Text;
                    prm.SqlDbType = System.Data.SqlDbType.NVarChar;
                    prm.Size = 15;
                    cmd.Parameters.Add(prm);

                    prm = new SqlParameter();
                    prm.ParameterName = "@Date";
                    DateTime parsDate = new DateTime();
                    IsparsDate = DateTime.TryParse(txt_DateNext.Text, out parsDate);
                    if (IsparsDate == true)
                    {
                        prm.Value = parsDate;
                        prm.SqlDbType = System.Data.SqlDbType.Date;
                        cmd.Parameters.Add(prm);
                    }
                    else
                    {
                        txt_DateNextError.Text = "* dd/mm/yyyy";
                    }


                    prm = new SqlParameter();
                    prm.ParameterName = "@Time";
                    TimeSpan parsTime = new TimeSpan();
                    IsparsTime = TimeSpan.TryParse(txt_TimeNext.Text, out parsTime);
                    if (IsparsTime == true)
                    {
                        prm.Value = parsTime;
                        prm.SqlDbType = System.Data.SqlDbType.Time;
                        cmd.Parameters.Add(prm);
                    }
                    else
                    {
                        txt_TimeNextError.Text = "* hh:mm";
                    }

                    prm = new SqlParameter();
                    prm.ParameterName = "@TicketPrice";
                    prm.Value = Convert.ToDecimal(txt_TicketPrice.Text);
                    prm.SqlDbType = System.Data.SqlDbType.Money;
                    cmd.Parameters.Add(prm);

                    prm = new SqlParameter();
                    prm.ParameterName = "@IDshow";
                    prm.Value = txt_IDshowNext.Text;
                    prm.SqlDbType = System.Data.SqlDbType.Int;
                    cmd.Parameters.Add(prm);

                    if (IsparsTime == true && IsparsDate == true)
                    {
                        try
                        {
                            conn.Open();
                            cmd.ExecuteNonQuery();
                            msgBox("Update successfully!");
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Update was failes: " + ex.Message);
                        }
                    }
                }
            }
            if (IsparsTime == true && IsparsDate == true)
            {
                tbl_IUnext.Visible = false;
                btnInsert_TblNext.Visible = true;
                btnUpdate_TblNext.Visible = true;
                btnDel_TblNext.Visible = true;

                txt_IDshowNext.Text = "";
                txt_PerformerNext.Text = "";
                txt_ImageNext.Text = "";
                txt_PlaceNext.Text = "";
                txt_CityNext.Text = "";
                txt_CountryNext.Text = "";
                dd_DayNext.ClearSelection();
                txt_DateNext.Text = "";
                txt_TimeNext.Text = "";
                txt_TicketPrice.Text = "";

                string sql1 = "SELECT * FROM Table_AllNextShow ORDER BY Date;";
                using (SqlConnection conn = new SqlConnection(CS))
                {
                    SqlDataAdapter adptr = new SqlDataAdapter(sql1, conn);
                    DataTable dt = new DataTable();
                    adptr.Fill(dt);
                    gvAllNextShow.DataSource = null;
                    gvAllNextShow.DataSource = dt;
                    gvAllNextShow.DataBind();
                    for (int i = 0; i < gvAllNextShow.Rows.Count; i++)
                    {
                        lblNextshowsPanel.Text = "Total: " + i;
                    }
                    Cache["Table_AllNextShow"] = dt;
                }
            }
        }
    }


    ///////////////////////////////////////////////////////////////////////////////////////


    //Tbl-Last:

    //-Delete(Last)
    protected void btnDel_TblLast_Click(object sender, EventArgs e)
    {
        int chkBoxToDelete = 0;
        string CS = ConfigurationManager.ConnectionStrings["DBconnShows"].ConnectionString;
        foreach (GridViewRow row in gvlastShows.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                CheckBox chkRow = (row.Cells[0].FindControl("ChkChooseShows_TblLast") as CheckBox);
                if (chkRow.Checked == true)
                {
                    chkBoxToDelete++;
                    int IDshow = Convert.ToInt32(gvlastShows.Rows[row.RowIndex].Cells[1].Text);

                    //Delete//
                    using (SqlConnection conn = new SqlConnection(CS))
                    {
                        string sql = "DELETE FROM Table_LastShows WHERE IDshow=@IDshow";
                        using (SqlCommand cmd = new SqlCommand(sql, conn))
                        {
                            SqlParameter prm = new SqlParameter();
                            prm.ParameterName = "@IDshow";
                            prm.Value = IDshow;
                            prm.SqlDbType = System.Data.SqlDbType.Int;
                            cmd.Parameters.Add(prm);
                            conn.Open();
                            try
                            {
                                cmd.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                throw new Exception("Delete wad failed: " + ex.Message);
                            }
                        }
                    }
                }
            }
        }
        if (chkBoxToDelete == 0)
        {
            msgBox("Choose an items to delete.");
        }
        else
        {
            msgBox("Delete successfully!");
            string sql1 = "SELECT * FROM Table_LastShows ORDER BY Performer;";
            using (SqlConnection conn = new SqlConnection(CS))
            {
                SqlDataAdapter adptr = new SqlDataAdapter(sql1, conn);
                DataTable dt = new DataTable();
                adptr.Fill(dt);
                gvlastShows.DataSource = null;
                gvlastShows.DataSource = dt;
                gvlastShows.DataBind();
                for (int i = 0; i < gvlastShows.Rows.Count; i++)
                {
                    lblLastshowsPanel.Text = "Total: " + i;
                }
                Cache["Table_LastShows"] = dt;
            }
        }
    }

    //-Insert(Last)
    protected void btnInsert_TblLast_Click(object sender, EventArgs e)
    {
        btnInsert_TblLast.Visible = false;
        btnUpdate_TblLast.Visible = false;
        btnDel_TblLast.Visible = false;
        tbl_IUlast.Visible = true;
        btn_UpdateToDBlast.Visible = false;
        btn_InsertToDBlast.Visible = true;

        txt_IDshowLast.Enabled = true;

        foreach (GridViewRow row in gvlastShows.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                CheckBox chkRow = (row.Cells[0].FindControl("ChkChooseShows_TblLast") as CheckBox);
                chkRow.Enabled = false;
                if (chkRow.Checked == true)
                {
                    chkRow.Checked = false;
                }
            }
        }
    }
    protected void btn_InsertToDBlast_Click(object sender, EventArgs e)
    {
        if (IsValid)
        {
            //Insert the items to the database:
            string CS = ConfigurationManager.ConnectionStrings["DBconnShows"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(CS))
            {
                string sql = "INSERT INTO Table_LastShows (IDshow, Performer, Image, City, Country, YouTubeLinq) VALUES (@IDshow, @Performer, @Image, @City, @Country, @YouTubeLinq)";
                using (SqlCommand cmd = new SqlCommand(sql, conn))
                {
                    //Defind the parametrs:

                    SqlParameter prm = new SqlParameter();
                    prm.ParameterName = "@IDshow";
                    prm.Value = txt_IDshowLast.Text;
                    prm.SqlDbType = System.Data.SqlDbType.Int;
                    cmd.Parameters.Add(prm);

                    prm = new SqlParameter();
                    prm.ParameterName = "@Performer";
                    prm.Value = txt_PerformerLast.Text;
                    prm.SqlDbType = System.Data.SqlDbType.NVarChar;
                    prm.Size = 50;
                    cmd.Parameters.Add(prm);

                    prm = new SqlParameter();
                    prm.ParameterName = "@Image";
                    prm.Value = txt_ImageLast.Text;
                    prm.SqlDbType = System.Data.SqlDbType.NVarChar;
                    cmd.Parameters.Add(prm);

                    prm = new SqlParameter();
                    prm.ParameterName = "@City";
                    prm.Value = txt_CityLast.Text;
                    prm.SqlDbType = System.Data.SqlDbType.NVarChar;
                    prm.Size = 50;
                    cmd.Parameters.Add(prm);

                    prm = new SqlParameter();
                    prm.ParameterName = "@Country";
                    prm.Value = txt_CountryLast.Text;
                    prm.SqlDbType = System.Data.SqlDbType.NVarChar;
                    prm.Size = 50;
                    cmd.Parameters.Add(prm);

                    prm = new SqlParameter();
                    prm.ParameterName = "@YouTubeLinq";
                    prm.Value = txt_YouTubeLinq.Text;
                    prm.SqlDbType = System.Data.SqlDbType.NVarChar;
                    cmd.Parameters.Add(prm);

                    try
                    {
                        conn.Open();
                        cmd.ExecuteNonQuery();
                        msgBox("Insert successfully!");
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Insert operation was failes." + ex.Message);
                    }

                }
            }
            tbl_IUlast.Visible = false;
            btnInsert_TblLast.Visible = true;
            btnUpdate_TblLast.Visible = true;
            btnDel_TblLast.Visible = true;

            txt_IDshowLast.Text = "";
            txt_PerformerLast.Text = "";
            txt_ImageLast.Text = "";
            txt_CityLast.Text = "";
            txt_CountryLast.Text = "";
            txt_YouTubeLinq.Text = "";

            string sql1 = "SELECT * FROM Table_LastShows ORDER BY Performer;";
            using (SqlConnection conn = new SqlConnection(CS))
            {
                SqlDataAdapter adptr = new SqlDataAdapter(sql1, conn);
                DataTable dt = new DataTable();
                adptr.Fill(dt);
                gvlastShows.DataSource = null;
                gvlastShows.DataSource = dt;
                gvlastShows.DataBind();
                for (int i = 0; i < gvlastShows.Rows.Count; i++)
                {
                    lblLastshowsPanel.Text = "Total: " + i;
                }
                Cache["Table_LastShows"] = dt;
            }
        }
    }

    //-Cancel(Last)
    protected void btn_CancelLast_Click(object sender, EventArgs e)
    {
        tbl_IUlast.Visible = false;
        btnInsert_TblLast.Visible = true;
        btnUpdate_TblLast.Visible = true;
        btnDel_TblLast.Visible = true;

        txt_IDshowLast.Text = "";
        txt_PerformerLast.Text = "";
        txt_ImageLast.Text = "";
        txt_CityLast.Text = "";
        txt_CountryLast.Text = "";
        txt_YouTubeLinq.Text = "";

        foreach (GridViewRow row in gvlastShows.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                CheckBox chkRow = (row.Cells[0].FindControl("ChkChooseShows_TblLast") as CheckBox);
                chkRow.Enabled = true;
            }
        }
    }

    //-Update(Last)
    protected void btnUpdate_TblLast_Click(object sender, EventArgs e)
    {
        int totalSelected = 0;
        CheckBox chkRow;
        foreach (GridViewRow row in gvlastShows.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                chkRow = (row.Cells[0].FindControl("ChkChooseShows_TblLast") as CheckBox);
                if (chkRow.Checked == true)
                {
                    totalSelected++;
                }
            }
        }
        if (totalSelected == 1)
        {
            foreach (GridViewRow row in gvlastShows.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    chkRow = (row.Cells[0].FindControl("ChkChooseShows_TblLast") as CheckBox);
                    chkRow.Enabled = false;
                    if (chkRow.Checked == true)
                    {
                        txt_IDshowLast.Text = row.Cells[1].Text;
                        txt_PerformerLast.Text = row.Cells[2].Text;
                        txt_ImageLast.Text = row.Cells[3].Text;
                        txt_CityLast.Text = row.Cells[4].Text;
                        txt_CountryLast.Text = row.Cells[5].Text;
                        txt_YouTubeLinq.Text = row.Cells[6].Text;
                    }
                }

            }
            btnInsert_TblLast.Visible = false;
            btnUpdate_TblLast.Visible = false;
            btnDel_TblLast.Visible = false;
            tbl_IUlast.Visible = true;
            btn_UpdateToDBlast.Visible = true;
            btn_InsertToDBlast.Visible = false;

            txt_IDshowLast.Enabled = false;
        }
        else
        {
            msgBox("Please choose one item to update.");
        }
    }
    protected void btn_UpdateToDBlast_Click(object sender, EventArgs e)
    {
        if (IsValid)
        {
            string CS = ConfigurationManager.ConnectionStrings["DBconnShows"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(CS))
            {
                string sql = "UPDATE Table_LastShows SET Performer=@Performer, Image=@Image, City=@City, Country=@Country, YouTubeLinq=@YouTubeLinq WHERE IDshow=@IDshow";
                using (SqlCommand cmd = new SqlCommand(sql, conn))
                {
                    //Defind the parametrs:
                    SqlParameter prm = new SqlParameter();
                    prm.ParameterName = "@Performer";
                    prm.Value = txt_PerformerLast.Text;
                    prm.SqlDbType = System.Data.SqlDbType.NVarChar;
                    prm.Size = 50;
                    cmd.Parameters.Add(prm);

                    prm = new SqlParameter();
                    prm.ParameterName = "@Image";
                    prm.Value = txt_ImageLast.Text;
                    prm.SqlDbType = System.Data.SqlDbType.NVarChar;
                    cmd.Parameters.Add(prm);

                    prm = new SqlParameter();
                    prm.ParameterName = "@City";
                    prm.Value = txt_CityLast.Text;
                    prm.SqlDbType = System.Data.SqlDbType.NVarChar;
                    prm.Size = 50;
                    cmd.Parameters.Add(prm);

                    prm = new SqlParameter();
                    prm.ParameterName = "@Country";
                    prm.Value = txt_CountryLast.Text;
                    prm.SqlDbType = System.Data.SqlDbType.NVarChar;
                    prm.Size = 50;
                    cmd.Parameters.Add(prm);

                    prm = new SqlParameter();
                    prm.ParameterName = "@YouTubeLinq";
                    prm.Value = txt_YouTubeLinq.Text;
                    prm.SqlDbType = System.Data.SqlDbType.NVarChar;
                    cmd.Parameters.Add(prm);

                    prm = new SqlParameter();
                    prm.ParameterName = "@IDshow";
                    prm.Value = txt_IDshowLast.Text;
                    prm.SqlDbType = System.Data.SqlDbType.Int;
                    cmd.Parameters.Add(prm);

                    try
                    {
                        conn.Open();
                        cmd.ExecuteNonQuery();
                        msgBox("Update successfully!");
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Update was failes: " + ex.Message);
                    }                   
                }
            }         
            tbl_IUlast.Visible = false;
            btnInsert_TblLast.Visible = true;
            btnUpdate_TblLast.Visible = true;
            btnDel_TblLast.Visible = true;

            txt_IDshowLast.Text = "";
            txt_PerformerLast.Text = "";
            txt_ImageLast.Text = "";
            txt_CityLast.Text = "";
            txt_CountryLast.Text = "";
            txt_YouTubeLinq.Text = "";

            string sql1 = "SELECT * FROM Table_LastShows ORDER BY Performer;";
            using (SqlConnection conn = new SqlConnection(CS))
            {
                SqlDataAdapter adptr = new SqlDataAdapter(sql1, conn);
                DataTable dt = new DataTable();
                adptr.Fill(dt);
                gvlastShows.DataSource = null;
                gvlastShows.DataSource = dt;
                gvlastShows.DataBind();
                for (int i = 0; i < gvlastShows.Rows.Count; i++)
                {
                    lblLastshowsPanel.Text = "Total: " + i;
                }
                Cache["Table_LastShows"] = dt;
            }
        }        
    }



    public void msgBox(String msg)
    {
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "Message Box", "<script language='javascript'>alert('" + msg + "')</script>");
    }
}
